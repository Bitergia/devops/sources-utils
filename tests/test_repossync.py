# -*- coding: utf-8 -*-
# SPDX-License-Identifier: GPL-3.-or-later
#
# Copyright (C) 2025 Fioddor Superconcentrado
#
# Purpose: Automatic tests for RepOSSync
#
# Design.: - Unittest as framework.
#          - Based on grimoirelab-perceval-sonarcube
#
# Authors:
#   - Fioddor Superconcentrado <fioddor@gmail.com>
#
# Installation:
#   1. Create virtual environment: `python3 -m venv testbed.venv`
#   2. Enable virtual environment: `source testbed.venv/bin/activate`
#   3. Install the dependencies (both runtime and development):
#      `pip3 install -r generic/repossync-requirements.txt`
#   4. Run test suite: `python3 -m unittest tests/test_*.py` or
#      `python3 tests/test_*.py`.
#
# Pending:
#   - Get rid of the DIRTY hack that patches sys.path.
#
#----------------------------------------------------------------------------------------------------------------------

import unittest                       # common usage: testing automation boilerplate.
import httpretty as mock              # for testing (for mocking APIs).
import json
import os


def read_file(filename, mode='r'):
    '''Taken from test_gitlab.

    Pending: - Remove (import instead).
             - Unused
    '''
    with open(
         os.path.join(os.path.dirname(os.path.abspath(__file__)), filename), mode
        ) as f:
        content = f.read()
    return content


class Utilities(unittest.TestCase):
    ''' Testing Utilities.'''

    def mock_pages( name , query , max_page ):
        '''Mocks a series of pages. Unused'''

        for p in range( max_page ):
            page = p + 1

            url = query
            if 0 < p:
                url += '?&page={}&per_page=3'.format( page )

            TST_DIR = 'data/'
            body_file = '{}{}.P{}.body.RS.json'.format( TST_DIR , name , page )
            head_file = '{}{}.P{}.head.RS.json'.format( TST_DIR , name , page )
            #print('DEBUG Mocking:', url, 'FROM', body_file, head_file, '...')

            mock.register_uri( mock.GET , url
                             , match_querystring=True
                             ,            status=200
                             ,              body=           read_file(body_file, mode='rb')
                             ,   forcing_headers=json.loads(read_file(head_file).replace( "'" , '"' ))
                             )
            #print( 'Mock set up for {}'.format(url) )



class TestGitLabRepositories_Smoke(unittest.TestCase):
    '''Smoke test to check the import of repossync and the Gitlab client

    Needs available internet and a valid token.
    '''

    @classmethod
    def setUpClass(self):
        self.targets = {
            'Salsa': {
                'domain'   : 'salsa.debian.org',
                'org_name' : 'freedombox-team',
                'token'    : 'a_token_for_salsa'
            },
            'Bitergia': {
                'domain'  : None,
                'org_name': 'bitergium/bm',
                'token'   : 'a_token_for_bitergia'
            },
            'Bitergia2': {
                'org_name': 'bitergium/bm',
                'token'   : 'a_token_for_bitergia'
            }
        }

    def min_test(self, target):

        print('Fetching repos...')
        backend = GitLabRepositories(target)
        repos = backend.fetch_repos()

        self.assertIsInstance(repos, list)
        for proj in (0, len(repos)-1):
            self.assertIsInstance(repos[proj], dict)
            for source_type, url in repos[proj].items():
                self.assertIn(source_type[:6], ('git', 'gitlab'), 'Unexpected datasource {}.'.format(source_type))
                self.assertEqual(url[:8], 'https://','URL with strange protocol. Not HTTPS.')
                self.assertNotEqual(-1, url.find('/'), 'Strange URL {} lacks any slash.'.format(url))
        #print( 'projects.json (var repos):')
        #print( json.dumps(repos, indent=4) )


    @unittest.skip('Token expired')
    def test_salsa(self):
        self.min_test(self.targets['Salsa'])

    @unittest.skip('Speed')
    def test_bitergia(self):
        self.min_test(self.targets['Bitergia'])

    @unittest.skip('Speed')
    def test_bitergia2(self):
        self.min_test(self.targets['Bitergia2'])


class TestGitLabRepositories_Basics(unittest.TestCase):
    '''Tests the basic Gitlab queries'''

    @classmethod
    def setUpClass(self):
        self.backend = GitLabRepositories({
            'domain'   : 'a.domain',
            'org_name' : 'irrelevant'
        })
        self.expected = {
            'groups_valid': {
               'url': 'https://a.domain/api/v4/groups/my%2Fendpoint',
               'status': 200,
               'body': '{"id":12345678}',
            },
            'groups_unknown': {
               'url': 'https://a.domain/api/v4/groups/unknown',
               'status': 404,
               'body': '{"message":"404 Group Not Found"}',
            },
            'users_valid': {
               'url': 'https://a.domain/api/v4/users?username=known',
               'status': 200,
               'body': '[{"id":12345678}]',
            },
            'users_unknown': {
               'url': 'https://a.domain/api/v4/users?username=unknown',
               'status': 200,
               'body': '[]',
            }
        }

    def verify_fetch(self, expected):
        '''Call _fetch() and verify expectations'''
        backend = GitLabRepositories({'domain':'irrelevant', 'org_name':'irrelevant'})
        res = backend._fetch(expected['url'])

        self.assertEqual(res.status_code, expected['status'])
        self.assertEqual(res.text       , expected['body'  ])
        #print(res.text)

    @mock.activate
    def test_fetch_group_id_valid(self):
        '''Querying for a valid group returns an id

        ...plus a lot of other unneded data. See file
        data/bitergium_bm.groups.P0.body.RS.txt
        '''
        mock.register_uri( mock.GET , self.expected['groups_valid']['url']
              , match_querystring=True
              ,            status=self.expected['groups_valid']['status']
              ,              body=self.expected['groups_valid']['body']
              )
        self.verify_fetch(self.expected['groups_valid'])

    @mock.activate
    def test_fetch_group_id_unknown(self):
        '''Querying for an invalid group returns an HTTP404'''
        mock.register_uri( mock.GET , self.expected['groups_unknown']['url']
              , match_querystring=True
              ,            status=self.expected['groups_unknown']['status']
              ,              body=self.expected['groups_unknown']['body']
              )
        self.verify_fetch(self.expected['groups_unknown'])


    @mock.activate
    def test_group_id_valid(self):
        '''Querying for a valid group we get the id'''
        mock.register_uri( mock.GET , self.expected['groups_valid']['url']
              , match_querystring=True
              ,            status=self.expected['groups_valid']['status']
              ,              body=self.expected['groups_valid']['body']
              )
        self.assertEqual(self.backend.group_id('my%2Fendpoint'), 12345678)

    @mock.activate
    def test_group_id_unknown(self):
        '''Querying for an invalid group returns None'''
        mock.register_uri( mock.GET , self.expected['groups_unknown']['url']
              , match_querystring=True
              ,            status=self.expected['groups_unknown']['status']
              ,              body=self.expected['groups_unknown']['body']
              )
        self.assertIsNone(self.backend.group_id('unknown'))


    @mock.activate
    def test_users_valid(self):
        '''Querying for a valid user we get the id'''
        mock.register_uri( mock.GET , self.expected['users_valid']['url']
              , match_querystring=True
              ,            status=self.expected['users_valid']['status']
              ,              body=self.expected['users_valid']['body']
              )
        self.assertEqual(self.backend.user_id('known'), 12345678)

    @mock.activate
    def test_users_unknown(self):
        '''Querying for an invalid user returns None'''
        mock.register_uri( mock.GET , self.expected['users_unknown']['url']
              , match_querystring=True
              ,            status=self.expected['users_unknown']['status']
              ,              body=self.expected['users_unknown']['body']
              )
        self.assertIsNone(self.backend.user_id('unknown'))



class TestGitLabRepostories_IterRepositories(unittest.TestCase):
    '''Test _iter_repositories(url)

        See test data in `data/` folder.
        '''

    @classmethod
    def setUpClass(self):
        self.backend = GitLabRepositories({
            'domain'   : 'an.api',
            'org_name' : 'irrelevant'
        })

    @mock.activate
    def test_pagination(self):
        '''Test default pagination'''
        query = 'https://an.api/query'
        Utilities.mock_pages( 'repositories_group_paginated.txt' , query , 2 )

        repos = []
        for repo in self.backend._iter_repositories(query):
            repos.append(repo)
        self.assertTrue(len(repos) > 3)   # more results than just the first page.


    @mock.activate
    def test_digest_logic_default(self):
        '''Test the default digestion logic'''
        FIELDS = {
            'url': str,
            'fork': bool,
            'has_source': bool,
            'has_issues': bool,
            'has_merge': bool
        }
        query = 'https://an.api/query'
        Utilities.mock_pages( 'repositories_group_paginated.txt' , query , 2 )

        repos = []
        for repo in self.backend._iter_repositories(query):
            repos.append(repo)

        # Check expected types:

        self.assertIsInstance(repos[0], dict)

        for expected_element, expected_type in FIELDS.items():
            self.assertIn(expected_element, repos[0])
            self.assertIsInstance(repos[0][expected_element], expected_type)

        # Check expected values:

        self.assertTrue(repos[0]['has_source'])
        self.assertTrue(repos[0]['has_issues'])
        self.assertTrue(repos[0]['has_merge' ])

        # This case lacks both fields: `forked_from_project` and `repository_access_level`.
        # It also has both fields, `issues_enabled` and `merge_requests_enabled`, set to false.
        # Thus, all digested fields, except `url` must come set to False:
        case_result = repos[3]
        unexpected = FIELDS.copy()
        del unexpected['url']
        for field in unexpected:
           self.assertFalse(case_result[field])

        # 'archived' excluded by default:
        for repo in repos:
            self.assertFalse(repo['archived'])


    @mock.activate
    def test_digest_logic_policies(self):
        '''Test the logic of the policied digestion'''
        query = 'https://an.api/query'
        Utilities.mock_pages( 'repositories_group_paginated.txt' , query , 2 )

        # Test policy: allow archived:
        backend_archived = GitLabRepositories({
            'domain'   : 'an.api',
            'org_name' : 'irrelevant',
            'archived' : True
        })
        allow_archived = []

        for repo in backend_archived._iter_repositories(query):
            allow_archived.append(repo)

        self.assertFalse(allow_archived[0]['archived'])
        self.assertTrue( allow_archived[3]['archived'])

        # Test policy: ban forks:
        backend_noforks = GitLabRepositories({
            'domain'   : 'an.api',
            'org_name' : 'irrelevant',
            'forks'    : False
        })
        ban_forks = []

        for repo in backend_noforks._iter_repositories(query):
            ban_forks.append(repo)

        self.assertEqual(4, len(ban_forks))



class TestRemoveMissingRepos(unittest.TestCase):
    '''Test remove_missing_repos()'''

    @classmethod
    def setUpClass(self):
        self.previous_gh = {
                'git': [
                    'kept.git',
                    'kept_also.git',
                    'removed.git'
                ],
                'github2:issue': [
                    'kept',
                    'kept_also',
                    'removed'
                ],
                'github2:pull': [
                    'kept',
                    'kept_also',
                    'removed'
                ],
                'github:issue': [
                    'kept',
                    'kept_also',
                    'removed'
                ],
                'github:pull': [
                    'kept',
                    'kept_also',
                    'removed'
                ],
                'github:repo': [
                    'kept',
                    'kept_also',
                    'removed'
                ],
        }

        self.previous_gl = {
                'git': [
                    'kept.git',
                    'kept_also.git',
                    'removed.git'
                ],
                'gitlab:issue': [
                    'kept',
                    'kept_also',
                    'removed'
                ],
                'gitlab:merge': [
                    'kept',
                    'kept_also',
                    'removed'
                ],
        }

        self.new_repos_gh = [
            {
		'git'   : 'kept.git',
                'github': 'kept',
            },
            {
		'git'   : 'kept_also.git',
                'github': 'kept_also',
            }
        ]

        self.new_repos_gl = [
            {
		'git'         : 'kept.git',
                'gitlab:issue': 'kept',
                'gitlab:merge': 'kept',
            },
            {
		'git'         : 'kept_also.git',
                'gitlab:issue': 'kept_also',
                'gitlab:merge': 'kept_also',
            }
        ]


    def equalDicts(self, a:dict, b:dict) -> bool:
        '''Simple custom dict comparer'''
        if a.keys() != b.keys():
           return False
        for key in a.keys():
           if a[key] != b[key]:
              return False
        return True


    def expect_unknown(self, previous:dict):
        '''Expectation for an unknown policy in a scenario'''
        return copy.deepcopy(previous)


    def expect_keep(self, previous:dict):
        '''Expectation for Keep policy in a scenario'''
        return copy.deepcopy(previous)


    def expect_clean(self, previous:dict):
        '''Expectation for Clean policy in a scenario'''
        expect = copy.deepcopy(previous)
        for ds, rl in expect.items():
            if ds == 'git':
                rl.remove('removed.git')
            else:
                rl.remove('removed')
        return expect


    def expect_exclude(self, previous:dict):
        '''Expectation for Exclude policy in a scenario'''
        expect = copy.deepcopy(previous)
        for ds, rl in expect.items():
            if ds == 'git':
                rl.remove('removed.git')
                rl.append('removed.git --filter-no-collection=true')
            else:
                rl.remove('removed')
                rl.append('removed --filter-no-collection=true')
        return expect


    def test_policies_all(self):
        '''Test all policies for a list of scenarios'''
        scenarios = (
            (              {}, 'github', self.new_repos_gh),
            (self.previous_gh, 'github', self.new_repos_gh),
            (self.previous_gl, 'gitlab', self.new_repos_gl)
        )
        policies = {
            'Unknown': self.expect_unknown,
            KEEP     : self.expect_keep,
            CLEAN    : self.expect_clean,
            EXCLUDE  : self.expect_exclude
        }
        print('.')                     # Prepare for readable output to stdout
        for scenario in scenarios:
            for action, expect in policies.items():
                original = scenario[0]
                result = copy.deepcopy(original)
                source_type = scenario[1]
                new_repos = scenario[2]

                remove_missing_repos(
                    result, 'a_project', source_type, new_repos, action=action
                )

                if not self.equalDicts(result, expect(original)):
                   print(
                       'scenario:', json.dumps(scenario, indent=4),
                       '; policy:', action,
                       '; result:', json.dumps(result, indent=4),
                       '; expect:', json.dumps(expect(original), indent=4)
                   )
                   self.assertEqual(True,False)


    def test_interferences(self):
        '''Test datasource interferences'''

        def _craft_previous(raw:dict, prefix:str):
            '''Crafts a mergeable version of raw previous'''
            output = {}
            for ds, repos in raw.items():
                output[ds] = ['{}_{}'.format(prefix, r) for r in repos]
            return output

        def _build_original():
            '''Merge standard gh and gl previouses'''
            gh = _craft_previous(self.previous_gh, 'gh')
            gl = _craft_previous(self.previous_gl, 'gl')
            original={'meta':{'a':'A', 'b':'B'}}
            original.update(gh)
            original.update(gl)                                   # smashes git
            original['git'] = gh['git'] + gl['git']               # fixes git
            return original

        def _craft_new_repos(raw:list, prefix:str):
            '''Crafts a mergeable version of raw new_repos'''
            output = []
            for repo in raw:
                output.append({ds:'{}_{}'.format(prefix, url) for ds, url in repo.items()})
            return output

        # Test preparations:

        original = _build_original()
        #print('BEFORE', json.dumps(original, indent=4))
        nr_gh = _craft_new_repos(self.new_repos_gh, 'gh')
        nr_gl = _craft_new_repos(self.new_repos_gl, 'gl')

        # Interfere combining different sequences and policies

        result_AB = copy.deepcopy(original)
        remove_missing_repos(result_AB, 'a_project', GITHUB, nr_gh, action=EXCLUDE)
        remove_missing_repos(result_AB, 'a_project', GITLAB, nr_gl, action=CLEAN)
        #print('Debug after GL, Clean:', json.dumps(result_AB, indent=4))

        result_BA = copy.deepcopy(original)
        remove_missing_repos(result_BA, 'a_project', GITLAB, nr_gl, action=EXCLUDE)
        remove_missing_repos(result_BA, 'a_project', GITHUB, nr_gh, action=CLEAN)

        result_CC = copy.deepcopy(original)
        remove_missing_repos(result_CC, 'a_project', GITLAB, nr_gl, action=CLEAN)
        remove_missing_repos(result_CC, 'a_project', GITHUB, nr_gh, action=CLEAN)

        # Checks:

        # AC-1: other sources remain untouched
        for result in (result_AB, result_BA, result_CC):
            self.assertTrue( result['meta'])
            self.assertEqual(result['meta'],{'a':'A', 'b':'B'})

        # AC.2: miss cleaned, but not excluded:
        self.assertEqual(len(result_AB['git']), len(original['git'])-1)
        self.assertEqual(len(result_BA['git']), len(original['git'])-1)
        self.assertEqual(len(result_CC['git']), len(original['git'])-2)

        self.assertEqual(len(result_AB['gitlab:issue']), len(original['gitlab:issue'])-1)
        self.assertEqual(len(result_AB['github:issue']), len(original['github:issue'])-0)

        self.assertEqual(len(result_BA['gitlab:issue']), len(original['gitlab:issue'])-0)
        self.assertEqual(len(result_BA['github:issue']), len(original['github:issue'])-1)

        self.assertEqual(len(result_CC['gitlab:issue']), len(original['gitlab:issue'])-1)
        self.assertEqual(len(result_CC['github:issue']), len(original['github:issue'])-1)

        # AC-3: (mis)Matches:
        self.assertEqual(result_AB['gitlab:issue'], result_CC['gitlab:issue'])
        self.assertEqual(result_BA['github:issue'], result_CC['github:issue'])

        self.assertTrue(result_AB['github:issue'] != result_CC['github:issue'])
        self.assertTrue(result_BA['gitlab:issue'] != result_CC['gitlab:issue'])



class TestCapture(unittest.TestCase):
    '''Capture API responses

    Capture headers and body of the response from each `url`. Save in in `DIR\{}.*`.
    '''

    Map = [
      {
        'url' : 'https://gitlab.com/api/v4/users/izubiaurre',
        'file' : 'users_username_valid.P0',
      },
      {
        'url' : 'https://gitlab.com/api/v4/users/desconocido',
        'file' : 'users_username_unknown.P0',
      },
      {
        'url' : 'https://gitlab.com/api/v4/groups/bitergium%2Fbm',
        'file' : 'bitergium_bm.groups.P0',
      },
      {
        'url' : 'https://gitlab.com/api/v4/groups/bitergium%2Fbm/projects',
        'file' : 'repositories_group.P0',
      },
      {
        'url' : 'https://gitlab.com/api/v4/users/izubiaurre/projects',
        'file' : 'repositories_user.P0',
      },
      {
        'url' : 'https://gitlab.com/api/v4/groups/bitergium%2Fbm/subgroups',
        'file' : 'subgroups_group',
      },
      {
        'url' : 'https://gitlab.com/api/v4/groups/bitergium%2Fbm/subgroups',
        'params': { 'page': 1, 'per_page': 3 },
        'file' : 'subgroups_group_paginated.P1',
      },
      {
        'url' : 'https://gitlab.com/api/v4/groups/bitergium%2Fbm/subgroups',
        'params': { 'page': 2, 'per_page': 3 },
        'file' : 'subgroups_group_paginated.P2',
      }
    ]

    @unittest.skip('WIP')
    def test_capture(self):
        print('Fetching repos...')
        backend = GitLabRepositories()
        repos = backend.fetch_repos( target )



if __name__ == "__main__":
    # Called as `python3 tests/test_*.py`
    # The test suite is a script depending on the software under test (SUT).

    print( 'Debug: Executing test_repossync as __main__ (called as ./script.py or as python3 script.py).' )
    print( '-' * 40 )

    import sys
    #print('Dependency hell DEBUG info:', '_'*30)
    #print('  1. Search for module name among the Built-in modules:', sys.builtin_module_names)
    #print('  2. Search for name.py file in sys.path, which is:', sys.path)
    #print('-'*40)

    # Read the current execution directory and append it to sys.path:
    full_path_me = os.path.realpath(__file__)
    full_path_my_folder = os.path.dirname(full_path_me)
    full_path_my_parent = full_path_my_folder[:full_path_my_folder.rfind('/')]
    full_path_repossync_folder = full_path_my_parent + '/generic'
    sys.path.append(full_path_repossync_folder)
    #print('  3. After DIRTY PATCH, new sys.path is:', sys.path)
    #print('-'*40)

    from repossync import *
    unittest.main(verbosity=3)
else:
    # Called e.g. as `python3 -m unittest tests/test_*.py`
    print( 'Debug: Executing test_repossync as "{}".'.format(__name__) )
    print( '-' * 40 )

    from generic.repossync import *
