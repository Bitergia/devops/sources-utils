From @jgb:

I updated the scripts, which now seem to work for all the intended datasources. In summary:

* spreadsheet_to_projects.py {F7107}. Run as (see details above, and using `--help`):

```
$ python3 spreadsheet_to_projects.py --projects projects.xlsx --json projects.json
```

* update_projects_3.py {F7108}. Run as (see details above, and using `--help`):

```
$ python update_projects_3.py --es https://bitergia:XXX@mozilla-test.biterg.io/data \
  --index_bugzilla bugzilla --projects projects.json
```
For the latter to work, you need to previously install the `elasticsearch` package:

```
$ pip install elasticsearch
```
