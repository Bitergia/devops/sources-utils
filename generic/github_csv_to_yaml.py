# Copyright (C) 2015-2020 Bitergia
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Authors:
#     Quan Zhou <quan@bitergia.com>
#


import argparse
import copy
import csv
import datetime
import yaml


COMMUNITY_MAINTAINERS = 'Community Maintainers'
PARENT_TEAM = 'Community Contributors'
PARTNER_CONTRIBUTOR = 'Partner Contributor'
INDIVIDUAL_CONTRIBUTOR = 'Individual Contributor'

TOP_PRIORITY = 'statistic-magento-community-skip-list'


def main():
    """The script processes a CSV file that contains a list of GitHub users
    and their memberships to GitHub teams. The output is a YAML file that can
    be ingested to SortingHat. GitHub users are translated to SortingHat profiles,
    GitHub teams to SortingHat organizations and GitHub memberships to SortingHat
    enrollments.

    When the profile name is duplicated it will use the GitHub username.

    If `whitelist` is defined in the conf file and `fallback` isn't provided,
    all teams in `whitelist` will be kept and the rest removed.
    If `fallback` is provided, the teams not in `whitelist` will be converted
    to the `fallback` SortingHat organization and kept.

    Configuration YAML file:
    - csv: input CSV file
    - affiliations: output affiliation YAML file
    - hierarchy: input hierarchy YAML file
    - gitlab <optional>: upload the affiliation and organization files to a GitLab repository
      - token: token
      - project_id: project ID
    - rename <optional>: rename GitHub team (old: new)
    - whitelist <optional>: filter only by these GitHub teams
    - fallback <optional>: set the name of the SortingHat organization for all GitHub teams not in whitelist
    - priority <optional>: when the team is in priority the other teams in the same profile will be removed
    - domains <optional>: emails with these doamins will enrolled to `Employee`
    - maintainer_teams <optional>: when the team is in maintainer_teams, it will be enrolled to
      COMMUNITY_MAINTAINERS.

    i.e configuration YAML file:
    ```
    csv: affiliations.csv
    affiliation: affiliations.yml
    hierarchy: hierarchy.yml
    gitlab:
      token: <token>
      project_id: <project_ID>
    rename:
      Bots: Automation
    whitelist:
      - DevOps
      - Research
    fallback: Engineer
    priority:
      - DevOps
    ```

    i.e. hierarchy YAML file:
    ```
    - Partners A:
      - A: []
      - B: []
    - Partners B:
      - C: []
      - D: []
    ```

    i.e. CSV file:
    ```
    github_login,name,email,github_org,github_team,first_date,last_date
    zhquan,Quan Zhou,quan@bitergia.com,chaoss,DevOps,1970-01-01,2100-01-01
    zhquan,Quan Zhou,quan@bitergia.com,chaoss,Engineer,1970-01-01,2100-01-01
    ```

    i.e affiliations YAML file:
    ```
    - email:
      - quan@bitergia.com
      enrollments:
      - end: 2100-01-01
        organization: DevOps
        start: 1970-01-01
      github:
      - zhquan
      profile:
        name: Quan Zhou
    ```

    Execution:
    ```
    $ python3 <conf.yml>
    ```
    """
    args = parse_args()
    conf_name = args.conf

    conf = read_yaml(conf_name)
    csv_name = conf['csv']
    affiliations_yaml_name = conf['affiliation']
    hierarchy_yaml_name = conf['hierarchy']

    archive = read_csv(csv_name)

    fallback_org = conf.get('fallback', None)
    domains = conf.get('domains', None)
    rename = conf.get('rename', None)
    whitelist_teams = conf.get('whitelist', None)
    maintainer_teams = conf.get('maintainer_teams', None)
    if whitelist_teams:
        if rename:
            # Add the renamed teams into the whitelist_teams
            whitelist_teams += list(rename.keys())
        if maintainer_teams:
            whitelist_teams += maintainer_teams
        # Add the hierarchy teams into the whitelist_teams
        hierarchy = read_yaml(hierarchy_yaml_name)
        keys = get_keys(hierarchy)
        whitelist_teams += keys

        # if fallback_org isn't set all the teams not in whitelist_teams are removed.
        # Otherwise, the teams outside whitelist_teams are set to fallback_org and kept.
        archive = apply_whitelist(archive, whitelist_teams, fallback_org)

    affiliations_yaml_file = to_yaml(archive)

    priority_teams = conf.get("priority", None)
    if priority_teams:
        affiliations_yaml_file = apply_priority_yaml(affiliations_yaml_file, priority_teams)

    if maintainer_teams:
        # If the team is in maintainer_teams add COMMUNITY_MAINTAINERS
        affiliations_yaml_file = add_maintainer_teams(affiliations_yaml_file, maintainer_teams)

    if rename:
        # Rename the team according to the conf
        affiliations_yaml_file = rename_team(affiliations_yaml_file, rename)

    if fallback_org:
        affiliations_yaml_file = clean_fallback_yaml(affiliations_yaml_file, fallback_org, domains)

    affiliations_yaml_file = add_child_teams_yaml(affiliations_yaml_file)
    affiliations_yaml_file = clean_duplicated_teams_yaml(affiliations_yaml_file)
    affiliations_yaml_file = fix_duplicate_name_yaml(affiliations_yaml_file)

    write_yaml(affiliations_yaml_name, affiliations_yaml_file)


def parse_args():
    """Parse command line arguments"""

    parser = argparse.ArgumentParser()
    parser.add_argument("conf",
                        help="YAML config file")

    return parser.parse_args()


def get_keys(data, output=[]):
    """Get all keys from a list of dictionaries.

    :param data: data
    :param output: output

    :return: list of the keys
    """

    keys = list(set().union(*(d.keys() for d in data)))
    if not keys:
        return output
    for d in data:
        key = list(d.keys())[0]
        output += get_keys(d[key], keys)

    unique_list = sorted(list(set(output)))
    return unique_list


def read_yaml(filename):
    with open(filename) as file:
        data = yaml.load(file, Loader=yaml.FullLoader)
    return data


def read_csv(file):
    """Read a CSV file with the following format:
    github_login,name,email,github_org,github_team,first_date,last_date

    And returns a list of items with the following format:
    [
        {'github_login': <login>},
        {'name': <name>},
        {'email': <email>},
        {'github_org': <org>},
        {'github_team': <team>},
        {'first_date': <datetime>},
        {'last_date': <datetime>}
    ]

    :return: list of users info
    """
    data = []
    with open(file, newline='') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line = 0
        for row in csv_reader:
            if line == 0:
                key = row
            else:
                user = {
                    key[0]: row[0],
                    key[1]: row[1],
                    key[2]: row[2],
                    key[3]: row[3],
                    key[4]: row[4],
                    key[5]: datetime.datetime.strptime(row[5], '%Y-%m-%d').date(),
                    key[6]: datetime.datetime.strptime(row[6], '%Y-%m-%d').date()
                }
                data.append(user)
            line += 1
    return data


def rename_team(data, rename):
    """Rename the GitHub team

    :param data: data
    :param rename: team to rename

    :return: data with teams renamed
    """
    aux = copy.deepcopy(data)
    for profile in aux:
        for enrolls in profile['enrollments']:
            team = enrolls['organization']
            if team in rename:
                enrolls['organization'] = rename[team]

    return aux


def apply_whitelist(data, whitelist_teams, fallback_org=None):
    """If fallback_org is None, remove all GitHub teams not in whitelist_teams.
    If fallback_org is not None, all non whitelisted teams will be set to fallback_org and kept.

    :param data: data
    :param whitelist_teams: whitelist teams
    :param fallback_org: fallback organization

    :return: data filtered by whitelist
    """
    if not fallback_org:
        aux = [d for d in data if d['github_team'] in whitelist_teams]
        return aux

    aux = copy.deepcopy(data)
    for d in aux:
        team = d['github_team']
        if team not in whitelist_teams:
            d['github_team'] = fallback_org

    return aux


def to_yaml(data):
    new_yml = []
    logins = []
    for d in data:
        if new_yml:
            logins = {a['github'][0]: num for num, a in enumerate(new_yml, start=0)}
        if d['github_login'] in logins:
            new = {
                "organization": d["github_team"],
                "start": d["first_date"],
                "end": d["last_date"]
            }
            index = logins[d['github_login']]
            if new not in new_yml[index]['enrollments'] and d["github_team"]:
                new_yml[index]['enrollments'].append(new)
        else:
            if d['github_team']:
                new = {
                    "profile": {},
                    "enrollments": [
                        {
                            "organization": d["github_team"],
                            "start": d["first_date"],
                            "end": d["last_date"]
                        }
                    ],
                    "github": [d['github_login']]
                }
                if d["email"]:
                    new['email'] = [d['email']]
                new['profile']['name'] = d['name'] if d['name'] else d['github_login']
                new_yml.append(new)

    return new_yml


def clean_fallback_yaml(data, fallback, domains):
    """Remove fallback organization from data when there are more than one.

    In case all organizations are fallback it will remove all of them and
    add a new fallback one.

    :param data: data
    :param fallback: fallback
    :param domains: domains

    :return: cleaned data
    """
    def check_domain(profile, domains):
        if 'email' in profile and profile['email'][0].split("@")[1] in domains:
            return 'Employee'
        else:
            return 'Community Contributors'

    aux = copy.deepcopy(data)
    for d in aux:
        enrolls = d['enrollments']
        if len(enrolls) == 1:
            if d['enrollments'][0]['organization'] == fallback:
                d['enrollments'][0]['organization'] = check_domain(d, domains)
            continue
        aux_enrolls = [e for e in enrolls if e['organization'] != fallback]
        if not aux_enrolls:
            # when all organizations are fallback we have to add one fallback org.
            aux_enrolls = [{
                'organization': fallback,
                'start': datetime.date(1970, 1, 1),
                'end': datetime.date(2100, 1, 1)
            }]
        d['enrollments'] = aux_enrolls
        if d['enrollments'][0]['organization'] == fallback:
            d['enrollments'][0]['organization'] = check_domain(d, domains)

    return aux


def add_child_teams_yaml(affiliations_yaml_file):
    """ Add child teams INDIVIDUAL_CONTRIBUTOR or PARTNER_CONTRIBUTOR

    :param affiliations_yaml_file: affiliations YAML file

    :return: affiliations YAML file with child teams
    """
    aux = copy.deepcopy(affiliations_yaml_file)
    for profile in aux:
        enrolls = [enroll['organization'] for enroll in profile['enrollments']]
        if PARENT_TEAM not in enrolls:
            continue
        if len(enrolls) == 1:
            individual = copy.deepcopy(profile['enrollments'][0])
            individual["organization"] = INDIVIDUAL_CONTRIBUTOR
            profile['enrollments'].append(individual)
            continue
        for enroll in profile['enrollments']:
            if enroll['organization'] == PARENT_TEAM:
                partner = copy.deepcopy(enroll)
                break
        partner["organization"] = PARTNER_CONTRIBUTOR
        profile['enrollments'].append(partner)
    return aux


def clean_duplicated_teams_yaml(affiliations_yaml_file):
    """Remove duplicate enrollments

    :param affiliations_yaml_file: affiliation

    :return: affiliation without duplicated
    """
    aux = copy.deepcopy(affiliations_yaml_file)
    for profile in aux:
        no_duplicated = []
        orgs = []
        for enroll in profile['enrollments']:
            if enroll['organization'] not in orgs:
                no_duplicated.append(enroll)
                orgs.append(enroll['organization'])
            else:
                update_duplicate_team_date(no_duplicated, enroll)
        profile['enrollments'] = no_duplicated

    return aux


def update_duplicate_team_date(data, duplicated_enroll):
    """Update duplicate team date using the widest.

    :param data: data
    :param duplicated_enroll: duplicated data
    :return: data updated
    """

    min_date = duplicated_enroll['start']
    max_date = duplicated_enroll['end']
    enroll = [enroll for enroll in data if enroll['organization'] == duplicated_enroll['organization']][0]
    if min_date < enroll['start']:
        enroll['start'] = min_date
    if max_date > enroll['end']:
        enroll['end'] = max_date


def fix_duplicate_name_yaml(affiliations_yaml_file):
    """ When the name is duplicated it will use the GitHub
    username instead of. But if two GitHub usernames use the
    same name and one of them starts with 'engcom-' it will
    take precedence in using the name field.

    :param affiliations_yaml_file: affiliation file
    :return: affiliation file without duplicated names
    """
    aux = copy.deepcopy(affiliations_yaml_file)
    unique_names = {}
    to_remove = []
    for i, profile in enumerate(aux):
        name = profile['profile']['name']
        if ' ' in name:
            continue
        elif name not in unique_names.keys():
            unique_names[name] = i
            continue
        login = profile['github'][0]
        if 'engcom-' in login:
            index = unique_names[name]
            new_name = aux[index]['github'][0]
            aux[index]['profile']['name'] = new_name
            unique_names[new_name] = index
            unique_names[name] = i
            continue
        profile['profile']['name'] = login

        for i in to_remove:
            del aux[i]

    return aux


def apply_priority_yaml(data, priority_teams):
    """When the team is in priority the other teams in the same profile
    will be removed.

    :param data: data
    :param priority_teams: priority teams

    :return: data with priority
    """
    aux = copy.deepcopy(data)
    for profile in aux:
        enrolls = [e['organization'] for e in profile['enrollments']]
        intersection = set(enrolls).intersection(set(priority_teams))
        if TOP_PRIORITY in intersection:
            intersection = [TOP_PRIORITY]
        if not intersection:
            continue
        priority_orgs = {
            e['organization']: {
                'start': e['start'],
                'end': e['end']
            } for e in profile['enrollments'] if e['organization'] in intersection}
        first_date, last_date = get_first_last_date(priority_orgs)

        enrolls_fixed = fix_overlap_date(first_date, last_date, priority_orgs, profile)
        profile['enrollments'] = enrolls_fixed

    return aux


def add_maintainer_teams(data, maintainers):
    """ Add COMMUNITY_MAINTAINERS org when the org is one of the maintainers

    :param data: data
    :param maintainers: list of maintainers

    :return: data with maintainer
    """
    aux = copy.deepcopy(data)
    for profile in aux:
        maintainer_enrolls = [enroll for enroll in profile['enrollments'] if enroll['organization'] in maintainers]
        for enroll in maintainer_enrolls:
            new = copy.deepcopy(enroll)
            new['organization'] = COMMUNITY_MAINTAINERS
            profile['enrollments'].append(new)

    return aux


def fix_overlap_date(first_date, last_date, priority_orgs, profile):
    """Fix overlap date when two or more orgs they date are overlapped.
    The orgs in priority_orgs have priority when the date are overlapped,
    then the other orgs will adapt the start and the last date accordingly.

    :param first_date: very first date of the priority orgs
    :param last_date: very last date of the priority orgs
    :param priority_orgs: priority orgs
    :param profile: profile to fix

    :return: enrollments fixed
    """
    enrolls = []
    for p in profile['enrollments']:
        if p['organization'] in priority_orgs:
            enrolls.append(p)
        else:
            if p['start'] < first_date < p['end']:
                p['end'] = first_date
            if p['start'] < last_date < p['end']:
                p['start'] = last_date
            if p['start'] >= first_date and p['end'] <= last_date:
                continue
            enrolls.append(p)
    return enrolls


def get_first_last_date(orgs):
    """Get the very first and last date of the organizations

    :param orgs: organizations

    :return: first date, last date
    """
    first_date = None
    last_date = None
    for org in orgs:
        if not first_date and not last_date:
            first_date = orgs[org]['start']
            last_date = orgs[org]['end']
            continue
        if first_date > orgs[org]['start']:
            first_date = orgs[org]['start']
        if last_date < orgs[org]['end']:
            last_date = orgs[org]['end']

    return first_date, last_date


def write_yaml(file_name, data):
    with open(file_name, 'w') as file:
        yaml.Dumper.ignore_aliases = lambda *args: True
        yaml.dump(data, file, allow_unicode=True, default_flow_style=False)


if __name__ == '__main__':
    main()
