# get_channel_mattermost.py

TODO

# get_channel_slack.py

TODO

# get_n_identities.py

Fetch hatstall_uuid, name, the ID number with the
same UUID, email associated from SortingHat database.

The parameter `limit` is the ID number with the same UUID.
i.e if `limit` is 10 it will get all the UUID with more
than 9 associated IDs.

The script generate a CSV file with the following format:
```
haststall_uuid,name,n
http://localhost:5601/identities/hatstall/030a8ba106db40fd3964af218029a29fdd6e8be8,Quan Zhou,25
```

Execution:
```
python3 get_n_identities.py localhost test_sh 20
```

## execute_identities.py

Fetch the number of the identities affiliated to a unique identity and
the emails associated in a MySQL/MaraiDB database. And create a comment
in phabricator if the limit is exceeded.

You can set a default limit for all of them and customize some limit
in the JSON config file:
```
{
    "phab_host": "<PHAB_host>",
    "phab_token": "<PHAB_token>",
    "phab_task_id": "<PHAB_task_ID>",
    "host": "<MySQL_host>",
    "user": "<MySQL_user>",
    "password": "<MySQL_password>",
    "limit_default": 30,
    "limits": {
        "bitergia_sh": 20,
        "chaoss_sh": 10
    }
}
```

i.e. phabricator comment
```
Database: biteriga_sh, limit: 15
<hatstall_URL>/f82f2fbef236f4a5909e726046ddfc8494994f42	Quan Zhou	16	['quan@bitergia.com', 'quanzh@bitergia.com']	2
<hatstall_URL>/5678bf8765d4567897654231bbbfff1234253213	Tom	17	['tom@tom.com']	1
```

Execution
```
$ python3 execute_identities.py conf.json
```

# github_csv_to_yaml.py

The script processes a CSV file that contains a list of GitHub users
and their memberships to GitHub teams. The output is a YAML file that can
be ingested to SortingHat. GitHub users are translated to SortingHat profiles,
GitHub teams to SortingHat organizations and GitHub memberships to SortingHat
enrollments.

When the profile name is duplicated it will use the GitHub username.

If `whitelist` is defined in the conf file and `fallback` isn't provided,
all teams in `whitelist` will be kept and the rest removed.
If `fallback` is provided, the teams not in `whitelist` will be converted
to the `fallback` SortingHat organization and kept.

Configuration YAML file:
- csv: input CSV file
- affiliations: output affiliation YAML file
- hierarchy: input hierarchy YAML file
- rename <optional>: rename GitHub team (old: new)
- whitelist <optional>: filter only by these GitHub teams
- fallback <optional>: set the name of the SortingHat organization for all GitHub teams not in whitelist
- priority <optional>: when the team is in priority the other teams in the same profile will be removed
- domains <optional>: emails with these doamins will enrolled to `Employee`

i.e configuration YAML file:
```
csv: affiliations.csv
affiliation: affiliations.yml
hierarchy: hierarchy.yml
gitlab:
  token: <token>
  project_id: <project_ID>
rename:
  Bots: Automation
whitelist:
  - DevOps
  - Research
fallback: Engineer
priority:
  - DevOps
```

i.e. hierarchy YAML file:
```
- Partners A:
  - A: []
  - B: []
- Partners B:
  - C: []
  - D: []
```

i.e. CSV file:
```
github_login,name,email,github_org,github_team,first_date,last_date
zhquan,Quan Zhou,quan@bitergia.com,chaoss,DevOps,1970-01-01,2100-01-01
zhquan,Quan Zhou,quan@bitergia.com,chaoss,Engineer,1970-01-01,2100-01-01
```

i.e affiliations YAML file:
```
- email:
  - quan@bitergia.com
  enrollments:
  - end: 2100-01-01
    organization: DevOps
    start: 1970-01-01
  github:
  - zhquan
  profile:
    name: Quan Zhou
```

Execution:
```
$ python3 <conf.yml>
```

# github_team_info.py

Fetch information from GitHub users belonging to organization teams.
From that information, a CSV and hierarchy of COMMUNITY_CONTRIBUTORS YAML files
are produced where the organization and team membership is updated over time,
updating the dates when the changes occur.

The argument `employees` is the `magento-employees` members CSV file name and the
team EMPLOYEES_TEAM (`magento-employees`) is skipped.

The CSV files will be uploaded to the GitLab repository.

The very first execution will set the `first_date` to `1970-01-01` and the
`last_date` to `2100-01-01`. The next executions will update those dates
according to the changes.

i.e. CSV file:
```
github_login,name,email,github_org,github_team,first_date,last_date
zhquan,Quan Zhou,quan@bitergia.com,chaoss,grimoirelab,1900-01-01,2100-01-01
```

i.e. hierarchy YAML file:
```
- Partners A:
  - A: []
  - B: []
- Partners B:
  - C: []
  - D: []
```

Execution:
```
$ python3 github_team_info.py <github-token> employees.csv affiliations.csv hierarchy.yml magento <gitlab-token> 17276563 --print
```

## Requirements
- GitHub Token: only `mining-magento` has permission to access magento repositories.
- GitLab Token:
  - For test: `read` permission (and comment `gitlab_upload()` on 121 and 122 to avoid upload the files).
  - For production: `read and write` permissions.

# githubsync.py

This script synchronizes the repositories of the projects.json (hosted in GitLab) with the GitHub repositories. 

GitLab: `https://gitlab.com/bitergia/c/<environment>/sources`.

You need to have a `gitlab token`, `gitlab project id` and `github organization` that you want to synchronize:
```
python3 githubsync.py <gitlab_token> <gitlab_project_id> <github_org>
```

If we want to execute it with many projects at once use the script `execute_githubsync.py` but you will need to have a JSON config file named `githubsync.json` in the same path as the script with the following format.
```
    {
        "bitergia": {
            "project_id": "<bitergia_gitlab_project_id>",
            "github": [
                {
                    "alias": "Bitergia",
                    "name": "bitergia"
                },
                {
                    "alias": "GrimoireLab",
                    "name": "grimoirelab"
                }
            ]
        },
        "chaoss": {
            "project_id": "<chaoss_gitlab_project_id>",
            "github": [
                {
                    "alias": "Chaoss",
                    "name": "chaoss"
                }
            ]
        },
        "gitlab_token": "<TOKEN>"
    }
```

# git_first_last_commit_date_to_yaml.py

Complete the affiliations YAML file with first and last commit date.

Fetch commit date from enriched index but only under these conditions:
- Enrolled to `Community Contributors` and `Employee`
- The domain of the commits is one of `magento.com`, `ebay.com`, `adobe.com`, or `transoftgroup.com`

For these affiliations will create an organization `Employee` and merge
with the rest of the organizations without overlapping.

Configuration YAML file:
- es_host: input ElasticSearch endpoint
- index: input git enriched index
- affiliations: input/output affiliations YAML file
- gitlab <optional>: upload the affiliation and organization files to a GitLab repository
  - token: token
  - project_id: project ID

i.e
```
es_host: https://localhost:9200
index: git_enriched
affiliations: affiliations.yml
gitlab:
  token: <GitLab_TOKEN>
  project_id: <GitLab_Project_ID>
```
The file `affiliations.yml` is created by the script `github_team_info.py` and `github_csv_to_yaml.py`

Before
```
- email:
  - quan@bitergia.com
  enrollments:
  - end: 2100-01-01
    organization: Community Contributors
    start: 1900-01-01
  - end: 2100-01-01
    organization: Individual Contributor
    start: 1900-01-01
  github:
  - zhquan
  profile:
    name: Quan zhou
```
After
```
- email:
  - quan@bitergia.com
  enrollments:
  - end: 2018-07-09
    organization: Community Contributors
    start: 1900-01-01
  - end: 2100-01-01
    organization: Community Contributors
    start: 2019-02-25
  - end: 2018-07-09
    organization: Individual Contributor
    start: 1900-01-01
  - end: 2100-01-01
    organization: Individual Contributor
    start: 2019-02-25
  - end: 2019-02-25
    organization: Employee
    start: 2018-07-09
  github:
  - zhquan
  profile:
    name: Quan zhou
```

Execute:
```
python3 git_first_last_commit_date_to_yaml.py <conf.yml>
```
# logs2phab.py

To fetch logs in ElasticSearch and create tickets in phabricator for each type of
errors found and set a priority depending on your configuration file (by default
is 5), the priorities are ordered from lowest(1) to highest(5).

If yoy want to change the time range you have to set the parameter `time` in
the configuration file, by default is `now-1h`.

This is the query that we use for fetch all errors in our logs in ElasticSearch:
```
{
    'query': {
        'bool': {
            'must': [
                {
                    'match': {
                        'type': 'ERROR'
                    }
                }
            ],
            'filter': [
                {
                    'range': {
                        '@timestamp': {
                            'gte': 'now-1h'
                        }
                    }
                }
            ]
        }
    }
}
```

JSON configuration file:
```
{
    "es_host": "http://localhost:9200",
    "index": "index_name",
    "time_range": "now-1d",
    "phab_host": "https://yourphabhost/api/",
    "phab_token": "api-phabtoken",
    "phab_phid": ["PHID-PROJ-phab_phid"],
    "priorities": {
        "requests.exceptions.HTTPError": 1,
        "dulwich.errors.GitProtocolError": 2
    }
}
```
i.e. phabricator ticket:
```
Title: [logs.bitergia.net] KeyError - Priority 5
```
Description:
```
This error has been found in the environments: ['bitergia']
Error enriching raw from meetup (https://meetup.com/): 'lat'
Traceback (most recent call last):
  File "/usr/local/lib/python3.5/dist-packages/grimoire_elk/elk.py", line 533, in enrich_backend
    enrich_count = enrich_items(ocean_backend, enrich_backend)
  File "/usr/local/lib/python3.5/dist-packages/grimoire_elk/elk.py", line 321, in enrich_items
    total = enrich_backend.enrich_items(ocean_backend)
  File "/usr/local/lib/python3.5/dist-packages/grimoire_elk/enriched/meetup.py", line 355, in enrich_items
    eitem = self.get_rich_item(item)
  File "/usr/local/lib/python3.5/dist-packages/grimoire_elk/enriched/enrich.py", line 93, in decorator
    eitem = func(self, *args, **kwargs)
  File "/usr/local/lib/python3.5/dist-packages/grimoire_elk/enriched/meetup.py", line 214, in get_rich_item
    "lat": event['venue']['lat'],
KeyError: 'lat'
```

Execution:
```
python3 ERROR_to_PHAB.py phab.json
```

# repossync.py

This script retrieves the projects.json located in GitLab, and
depending on the `source` (i.e., GitHub, GitLab or Gerrit) and the `org_name`
selected, it fetches the repos from the GitHub, GitLab or OpenDev APIs. Then,
it updates the projects.json with the new repos for the given `org_name`,
and finally commits the changes to the GitLab repository hosting
the projects.json with the commit message `COMMIT_MSG`.

Note that in case the `source` is GitHub and the section `github:repo`
is contained in the projects.json, the repos in that section are
automatically updated.

To add the credentials to the git repositories it is mandatory to enable
--github-token and --github-user.

Skips all GitHub repositories starting with `-`.

The argument `--github-search` will sync the repositories using this query
to search on GitHub (when this is activated it will ignore `--org-name`)

Example of executions:
```
    repossync
        --gitlab-token ...
        --gitlab-project-id ...
        --github-search move+language%3AMove&type=repositories
        --org-name starlingx
        --project-name StarlingX
        --filter-raw-url review.opendev.org
        --source gerrit

    repossync
        --gitlab-token ...
        --gitlab-project-id ...
        --org-name Bitergia
        --project-name Bitergia
        --source github
        --github-token ... (optional)
        --github-user ... (optional)

    repossync
        --gitlab-token ...
        --gitlab-project-id ...
        --org-name Bitergia
        --project-name Bitergia
        --source gitlab
```


If we want to execute it with many projects at once use the script `execute_repossync.py`
but you will need to have two JSON config file.

To add the credentials to the git repositories:
* For all git repositories on `GitHub` or `GitLab`, add the next params in the general JSON file:
  * `github_token`
  * `github_user`
  or
  * `gitlab_token`
  * `gitlab_user`
* For certain projects, add the next params under `github` or `gitlab` in the projects JSON file:
  * `token`
  * `user`

* JSON file with general configuration.
```
{
    "jenkins_host": "https://myjenkinshost/",
    "bot_token": "<GitLab-Token>",
    "gitlab_token": "<GitLab-Token>", (optional)
    "gitlab_user": "<GitLab-User>" (optional)
    "github_token": "<GitHub-Token>", (optional, but recommended)
    "github_user": "<GitHub-User>" (optional)
}
```

* JSON file with projects information.
```
{
    "<project>": {
        "archived": boolean (by default is true, optional)
        "forks": boolean (by default is false, optional)
        "removed": {keep,excluded,clean} (by default is keep, optional)
        "project_id": "<gitlab_project_id>",
        "github": [
            {
                "alias": "Bitergia",
                "name": "bitergia",
                "token": "<GitHub-Token>", (optional)
                "user": "<GitHub-User>" (optional)
                "archived": boolean (by default the same as "archived" at project level, optional)
                "forks": boolean (by default the same as "forks" at project level, optional)
                "removed": {keep,excluded,clean} (by default the same as "removed" at project level, optional)
            },
            {
                "alias": "Chaoss",
                "query": "chaoss&type=Repositories"
            }
        ],
        "gitlab": [
            {
                "alias": "Bitergia",
                "name": "bitergia"
            }
        ],
        "gerrit": [
            {
                "alias": "Chaoss",
                "name": "chaoss",
                "filter_raw_url": "review.opendev.org"
            }
        ]
    }
}
```

Execution:
`python3 execute_repossync.py conf.json projects.json`
