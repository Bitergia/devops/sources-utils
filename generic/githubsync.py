# -*- coding: utf-8 -*-
#
# Copyright (C) 2019 Bitergia
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# Authors:
#   Quan Zhou <quan@bitergia.com>
#


import argparse
import json

import requests
import gitlab


GITHUB_API_URL = "https://api.github.com"


class GitLabFileManager:

    def __init__(self, token):
        self.token = token
        self.gl = gitlab.Gitlab('https://gitlab.com', private_token=self.token)

    def load_file(self, project_id, filename):
        """Load file from GitLab.

        :param project_id: Gitlab project id
        :param filename: file name

        :return: file ("utf-8")
        """
        project = self.gl.projects.get(project_id)

        f = project.files.get(file_path=filename, ref='master')
        return f.decode().decode("utf-8")

    def upload_file(self, project_id, filename, content, message):
        """Upload a file to GitLab.

        :param project_id: Gitlab project id
        :param filename: file name
        :param content: content
        :param message: commit message
        """
        project = self.gl.projects.get(project_id)
        data = {
            "branch": "master",
            "commit_message": message,
            "actions": [
                {
                    'action': 'update',
                    'file_path': filename,
                    'content': content
                }
            ]
        }
        project.commits.create(data)

    @staticmethod
    def remove_no_exist_repo(project_file, org):
        git = "git"
        github = "github"
        if org not in project_file:
            return

        for repo in project_file[org][github]:
            try:
                get_data(repo)
            except requests.exceptions.HTTPError:
                project_file[org][github].remove(repo)
                project_file[org][git].remove(repo + ".git")

    @staticmethod
    def add_new_repos(project_file, org, ds_type, gh_projects):
        if org not in project_file:
            project_file[org] = {}

        for repo in gh_projects:
            if ds_type not in project_file[org]:
                project_file[org][ds_type] = []
            if repo not in project_file[org][ds_type]:
                project_file[org][ds_type].append(repo)

    def update_project_file(self, project_file, project_name, gh_projects,
                            remove=None):
        """ Update the JSON file with the new repositories
        and remove the repositories does not exist activating
        the remove parameter.

        :param project_file: projects.json
        :param project_name: the name of the project to update
        :param gh_projects: {"git": [git-repos], "github": [github-repos]}
        :param remove: remove the repositories that no longer exist
        """
        git = "git"
        github = "github"
        org = project_name

        # Check if the repositories exists
        if remove:
            self.remove_no_exist_repo(project_file, org)

        # Add new repositories
        self.add_new_repos(project_file, org, git, gh_projects[git])
        self.add_new_repos(project_file, org, github, gh_projects[github])


def parse_args():
    """ Parse command line arguments """

    parser = argparse.ArgumentParser()
    parser.add_argument("token",
                        help="GitLab token")
    parser.add_argument("project_id",
                        help="GitLab project id")
    parser.add_argument("github_org",
                        help="GitHub organization to sync")

    return parser.parse_args()


def get_data(url, headers=None):
    r = requests.get(url, headers=headers)
    r.raise_for_status()

    return r


def fetch_github_projects_list(org, token=None):
    """Fetch github repositories.

    :param org: github organization
    :param token: github personal token, by default is None
    :return: {"git": [git-repos], "github": [github-repos]}
    """
    headers = {}
    if token:
        headers = {'Authorization': 'token ' + token}

    url = GITHUB_API_URL + "/users/" + org + "/repos"
    page = 1
    last_page = 0

    url_page = url + "?page=" + str(page)
    r = get_data(url_page, headers)

    git_repos = fetch_items(r, 'clone_url')
    github_repos = fetch_items(r, 'html_url')

    if 'last' in r.links:
        last_url = r.links['last']['url']
        last_page = last_url.split('page=')[1]
        last_page = int(last_page)

    while page < last_page:
        page += 1
        url_page = url + "?page=" + str(page)
        r = get_data(url_page, headers)

        git_repos += fetch_items(r, 'clone_url')
        github_repos += fetch_items(r, 'html_url')

    return {"github": github_repos, "git": git_repos}


def fetch_items(r, key):
    """Fetch items depends of the key.

    :param r: requests
    :param key: name of the key that you want to fetch
    :returns: list of the items
    """
    items = []
    for repo in r.json():
        items.append(repo[key])

    return items


def main(gl_token, project_id, org, alias, gh_token=None, remove=None):
    """Sync the projects.json with github and upload to gitlab repository with
    the commit message "<org> sync by GitHubSync".

    :param gl_token: gitlab token
    :param project_id: gitlab project id
    :param org: github organization
    :param alias: alias of the github organization
    :param gh_token: github token
    :param remove: remove the repositories that no longer exist
    """
    gl = GitLabFileManager(gl_token)
    project_file = gl.load_file(project_id, "projects.json")
    project_file = json.loads(project_file)
    gh_projects = fetch_github_projects_list(org, gh_token)
    gl.update_project_file(project_file, alias, gh_projects, remove)
    project_file = json.dumps(project_file, indent=4)
    commit_msg = org + " sync by GitHubSync"
    gl.upload_file(project_id, "projects.json", project_file, commit_msg)


if __name__ == '__main__':
    args = parse_args()
    gitlab_token = args.token
    gitlab_project_id = args.project_id
    github_org = args.github_org

    main(gitlab_token, gitlab_project_id, github_org)
