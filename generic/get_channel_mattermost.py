# -*- coding: utf-8 -*-
#
# Copyright (C) 2018 Bitergia
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# Authors:
#   Quan Zhou <quan@bitergia.com>
#


import argparse
import json

import requests


def parse_args():
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter)

    parser.add_argument("--url",
                        action="store",
                        dest="url",
                        help="mattermost URL")
    parser.add_argument("-t", "--token",
                        action="store",
                        dest="token",
                        help="mattermost token")
    parser.add_argument("--user-id",
                        action="store",
                        dest="user_id",
                        help="user id")
    parser.add_argument("--team-id",
                        action="store",
                        dest="team_id",
                        help="team id")
    args = parser.parse_args()

    return args


def __get_payload(user_id):
    payload = {"user_id": user_id}
    return payload


def __get_headers(token):
    headers = {'Authorization': 'Bearer ' + token}
    return headers


def get_channels(m_url, token, team_id):
    """ Get all public channels for the specific team

    :param m_url: URL
    :param token: token
    :param team_id: team id
    :return: json with public channels
    """
    headers = __get_headers(token)
    url = m_url+'/api/v4/teams/' + team_id + '/channels'

    r = requests.get(url, headers=headers)
    return r.json()


def join_channel(m_url, token, channel_id, user_id):
    """ Join the channel, because otherwise we can not fetch messages with perceval

    :param m_url: URL
    :param token: token
    :param channel_id: channel id
    :param user_id: user id
    """
    headers = __get_headers(token)
    payload = __get_payload(user_id)
    url = m_url+'/api/v4/channels/'+channel_id+'/members'

    requests.post(url, headers=headers, data=json.dumps(payload))


def main():
    args = parse_args()
    token = args.token
    m_url = args.url
    team_id = args.team_id
    user_id = args.user_id

    channels_json = get_channels(m_url, token, team_id)

    for channel in channels_json:
        join_channel(m_url, token, channel['id'], user_id)


if __name__ == "__main__":
    main()
