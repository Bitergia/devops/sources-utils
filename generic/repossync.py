# -*- coding: utf-8 -*-
# SPDX-License-Identifier: GPL-3.-or-later
#
# Copyright (C) 2015-2025 Bitergia
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Purpose: See the help in parse_args().
#
# Usage: This can be used in 2 modes:
#        - as a library, by calling sync_projects().
#        - as a CLI executable. See the help in parse_args().
#
# Design: In this file you'll find:
#         - some generic subroutines,
#         - a class to deal with the storage system of the
#           projects.json file to be updated,
#         - several equivalent backend classes for different forge types,
#         - several functions part of the update process,
#         - and the command line pre-processing functions.
#
# Authors:
#   Quan Zhou <quan@bitergia.com>
#   Valerio Cosentino <valcos@bitergia.com>
#   Igor Zubiaurre <izubiaurre@bitergia.com>
#


import argparse
import copy
import datetime
import json
import logging
import sys
import time

import requests
import gitlab
import simplejson


GITLAB_URL = 'https://gitlab.com'

GITHUB_API_URL = "https://api.github.com"
SEARCH_REPO_URL = GITHUB_API_URL + "/search/repositories"

GIT = 'git'
GITHUB = 'github'
GITHUB_ISSUE = 'github:issue'
GERRIT = 'gerrit'
GITLAB = 'gitlab'
GITLAB_ISSUE = 'gitlab:issue'
GITLAB_MERGE = 'gitlab:merge'
GIT_EXTENSION = '.git'

KEEP = 'keep'
EXCLUDE = 'exclude'
CLEAN = 'clean'

MAIN_BRANCH = 'main'
MASTER_BRANCH = 'master'
UPDATE_ACTION = 'update'

COMMIT_MSG = "{} sync by RepOSSync"
PROJECTS_JSON = "projects.json"

FILTER_NO_COLLECTION = '{} --filter-no-collection=true'
FILTER_RAW_REPO = '{} --filter-raw=data.project:{}'
GERRIT_PROJECTS_URL = '{}/projects/'
ORG_REPOS = "{}/orgs/{}/repos"
USER_REPOS = "{}/users/{}/repos"
CHECK_OWNER = "{}/users/{}"
REPO_HTML_URL = 'html_url'

RATE_LIMIT_HEADER = "X-RateLimit-Remaining"
RATE_LIMIT_RESET_HEADER = "X-RateLimit-Reset"
MIN_RATE_LIMIT = 10


def datetime_utcnow():
    """Return the current date and time in UTC."""

    return datetime.datetime.now(datetime.timezone.utc)


def urijoin(*args):
    """Joins given arguments into a URI.

    Trailing and leading slashes are stripped for each argument.

    This code is based on a Rune Kaagaard's answer on Stack Overflow.
    See http://stackoverflow.com/questions/1793261 for more into. The
    code was licensed as cc by-sa 3.0.
    Code from grimoire_toolkit

    :params *args: list of arguments to join

    :return: a URI string
    """
    return '/'.join(map(lambda x: str(x).strip('/'), args))


def add_credentials(repo, user, token):
    """Add credentials to the repository

    :param repo: repository name
    :param user: user
    :param token: token

    :return: repository with credentials
    """
    cred = "://{}:{}@".format(user, token)
    repo_cred = repo.replace("://", cred)

    return repo_cred


class GitLabFileManager:
    """GitLab file manager.

    This class allows to load, upload and commit the projects.json to
    GitLab using the library python-gitlab (version == 1.9.0). Furthermore,
    it provides additional functionalities to remove and add repositories
    to the projects.json
    """
    def __init__(self, token):
        self.token = token
        self.gl = gitlab.Gitlab(GITLAB_URL, private_token=self.token)

    def load_file(self, project_id, filename):
        """Load a file from GitLab.

        :param project_id: Gitlab project id
        :param filename: file name

        :return: file ("utf-8")
        """
        project = self.gl.projects.get(project_id)

        try:
            f = project.files.get(file_path=filename, ref=MASTER_BRANCH)
            branch = MASTER_BRANCH
        except gitlab.GitlabGetError:
            f = project.files.get(file_path=filename, ref=MAIN_BRANCH)
            branch = MAIN_BRANCH
        return f.decode().decode("utf-8"), branch

    def upload_file(self, project_id, filename, content, message, branch):
        """Upload a file to GitLab.

        :param project_id: Gitlab project id
        :param filename: file name
        :param content: content
        :param message: commit message
        :param branch: git branch
        """
        project = self.gl.projects.get(project_id)
        data = {
            "branch": branch,
            "commit_message": message,
            "actions": [
                {
                    'action': UPDATE_ACTION,
                    'file_path': filename,
                    'content': content
                }
            ]
        }
        project.commits.create(data)


def exist_repo(repository, repo_list):
    repo = False
    git_extension = False
    repo_filter = False
    git_extension_filter = False

    if repository in repo_list:
        repo = True
    if repository + GIT_EXTENSION in repo_list:
        git_extension = True
    if FILTER_NO_COLLECTION.format(repository) in repo_list:
        repo_filter = True
    if FILTER_NO_COLLECTION.format(repository + GIT_EXTENSION) in repo_list:
        git_extension_filter = True

    if repo or git_extension or repo_filter or git_extension_filter:
        return True
    return False


def sleep_for_rate(response):
    """Sleep until the token is ready to be reused

    :param response: HTTP response
    """
    rate_limit_reset_ts = response.headers.get(RATE_LIMIT_RESET_HEADER, None)
    rate_limit = response.headers.get(RATE_LIMIT_HEADER, None)
    if rate_limit_reset_ts and rate_limit:
        rate_limit = int(rate_limit)
        rate_limit_reset_ts = int(rate_limit_reset_ts)
        current_time_ts = datetime_utcnow().replace(microsecond=0).timestamp() + 1
        time_to_reset = rate_limit_reset_ts - current_time_ts
        time_to_reset = 0 if time_to_reset < 0 else time_to_reset
        if time_to_reset and rate_limit < MIN_RATE_LIMIT:
            time.sleep(time_to_reset)


def fetch(url, headers=None, auth=None):
    """Fetch the data from an URL

    :param url: target URL
    :param headers: request's headers
    :param auth: credentials
    """
    try:
        r = requests.get(url, headers=headers, auth=auth)
        r.raise_for_status()
        sleep_for_rate(r)
    except requests.exceptions.HTTPError as error:
        if error.response.status_code == 403:
            sleep_for_rate(r)
            r = requests.get(url, headers=headers)
        elif error.response.status_code == 500:
            time.sleep(10)
            r = requests.get(url, headers=headers, auth=auth)
            r.raise_for_status()
        else:
            raise error
    return r


def add_repos(data, archived, forks):
    repos = []

    for repo in data:
        if repo["archived"] and not archived:
            continue
        fork = repo.get("fork", None)
        if fork and not forks:
            continue
        elif fork is None:
            msg = "Unexpected JSON format {}".format(repo)
            logging.error(msg)
            continue
        elif repo['name'].startswith("-"):
            # Skip all repositories that starts with '-'
            msg = "Skip repository that starts with '-' {}".format(repo)
            logging.warning(msg)
            continue

        repos.append(
            {
                GIT: repo[REPO_HTML_URL],
                GITHUB: repo[REPO_HTML_URL]
            }
        )

    return repos


class GitHubRepositories:
    """GitHub backend to fetch repositories

        Design: The fetch_repo() method is created dinamically at construction
                time.
    """

    def __init__(self, data):
        """Creates a GitHub backend

        :param data:
                  user: user, it can be `None`
                 token: token, it can be `None`
              org_name: organization name
                search: search query (when no org_name)
              archived: track (or skip) archived repositories
                 forks: track (or skip) fork repositories
        """
        self.token = data.get('token', None)
        self.archived = data.get('archived', True)
        self.forks = data.get('forks', False)
        self.org_name = data.get('org_name', None)
        self.user = data.get('user',None)
        self.search = data.get('search', None)
        self.headers = {}
        if self.token:
            self.headers = {'Authorization': 'token ' + self.token}
        if self.search:
            self.fetch_repos = self._fetch_search
        elif self.org_name:
            self.fetch_repos = self._fetch_repos
        else:
            raise Exception('FATAL: GitHub backend needs either a search or an organization')

    def _fetch_search(self):
        repos = []

        url = SEARCH_REPO_URL + "?q=" + self.search
        page = 1
        last_page = 0

        url_page = url + "&page=" + str(page)
        r = fetch(url_page, headers=self.headers)

        repos.extend(add_repos(r.json()['items'], self.archived, self.forks))

        if 'last' in r.links:
            last_url = r.links['last']['url']
            last_page = last_url.split('page=')[1]
            last_page = int(last_page)

        while page < last_page:
            page += 1
            url_page = url + "&page=" + str(page)
            r = fetch(url_page, headers=self.headers)

            repos.extend(add_repos(r.json()['items'], self.archived, self.forks))

        return repos

    def _fetch_repos(self):
        """Fetch GitHub repositories

        :return: a list of repos
        """

        repos = []

        url = ORG_REPOS.format(GITHUB_API_URL, self.org_name)
        if not self.is_github_org():
            url = USER_REPOS.format(GITHUB_API_URL, self.org_name)
        page = 1
        last_page = 0

        url_page = url + "?page=" + str(page)
        r = fetch(url_page, headers=self.headers)

        repos.extend(add_repos(r.json(), self.archived, self.forks))

        if 'last' in r.links:
            last_url = r.links['last']['url']
            last_page = last_url.split('page=')[1]
            last_page = int(last_page)

        while page < last_page:
            page += 1
            url_page = url + "?page=" + str(page)
            r = fetch(url_page, headers=self.headers)

            repos.extend(add_repos(r.json(), self.archived, self.forks))

        return repos

    def is_github_org(self):
        """Check if the GitHub owner is an Organization

        :return: True | False
        """
        url = CHECK_OWNER.format(GITHUB_API_URL, self.org_name)
        r = fetch(url, headers=self.headers)
        github_owner_type = r.json()["type"]
        is_org = False if github_owner_type == "User" else True

        return is_org


class GitLabRepositories:
    """GitLab backend to fetch owner repositories

    This class allows the fetch repositories from users or groups.
    """

    def __init__(self, data):
        """Creates a GitLab backend

        :param data:
                      user: user, it can be `None`
                     token: token, it can be `None`
                    domain: URL domain
                  org_name: subgroup path
                  archived: track (or skip) archived repositories
                     forks: track (or skip) fork repositories

        """
        self.api_token = data.get('token', None)
        self.domain = data['domain']
        self.org_name = data['org_name']
        self.user = data.get('user', None)
        self.headers = {}
        if self.api_token:
            self.headers = {"Authorization": "Bearer " + self.api_token}
        if not self.domain:
            self.domain = GITLAB_URL.split('//')[1]
        self.base_url = "https://{}/api/v4".format(self.domain)
        self.policy = {
            'archived' : data.get('archived', False),
            'forks'    : data.get('forks', True)
        }

    def _fetch(self, url, params=None):
        """Fetch the data from a given URL.

        :param url: link to the resource
        """
        r = requests.get(url, headers=self.headers, params=params)
        return r

    def user_id(self, username):
        """Check if a username is a GitLab user and return its ID.

        :param username: GitLab username

        :return: ID of the user if exists, else None.
        """
        endpoint=urijoin(self.base_url, 'users')
        res = self._fetch(url=endpoint, params={'username': username})
        if res.status_code == 404:
            logging.error('{} returns HTTP 404 for username `{}`'.format(endpoint, username))
            return None
        res.raise_for_status()
        users = res.json()
        if len(users) > 0:
            return users[0]['id']
        return None

    def group_id(self, name):
        """Check if a name is a GitLab group and return its ID.

        :param name: GitLab name

        :return: ID of the group if exists, else None.
        """
        res = self._fetch(url=urijoin(self.base_url, 'groups', name))
        if res.status_code == 404:
            return None
        else:
            res.raise_for_status()
        group = res.json()
        return group['id']

    def _iter_repositories(self, url):
        """Fetch repositories URLs using pagination from a given url"""

        while True:
            res = self._fetch(url)
            res.raise_for_status()
            for repo in res.json():
                repo_data = {
                    'url': repo['web_url'],
                    'archived': 'archived' in repo and repo['archived'],
                    'fork': 'forked_from_project' in repo,
                    'has_source': 'repository_access_level' in repo and repo['repository_access_level'] == 'enabled',
                    'has_issues': True if 'issues_enabled' not in repo else repo['issues_enabled'],
                    'has_merge': True if 'merge_requests_enabled' not in repo else repo['merge_requests_enabled']
                }
                if not self.policy['archived'] and repo_data['archived']:
                    continue
                if not self.policy['forks'] and repo_data['fork']:
                    continue
                yield repo_data
            if 'next' in res.links.keys():
                url = res.links['next']['url']
            else:
                break

    def _iter_subgroups(self, url):
        """Fetch subgroups id using pagination from a given url"""

        params = {'all_available': True}
        while True:
            res = self._fetch(url, params=params)
            res.raise_for_status()
            for group in res.json():
                yield group['id']
            if 'next' in res.links.keys():
                url = res.links['next']['url']
            else:
                break

    def fetch_user_repositories(self, user_id):
        """Fetch GitLab repositories from a user.

        This function fetch all the repositories from a GitLab owner.
        It returns a list of repositories indicating whether the repository
        is a fork, has issues enabled, or has merge requests enabled.

        :param user_id: id of the GitLab user

        :return: a list of repositories including url, has_issues and fork.
        """

        url = urijoin(self.base_url, 'users', user_id, 'projects')
        return self._iter_repositories(url)

    def fetch_group_repositories(self, group_id):
        """Fetch GitLab repositories from a group.

        This function fetch all the repositories from a GitLab group
        and subgroups in a recursive way.
        It returns an iterator with the list of repositories indicating
        whether the repository is a fork, has issues enabled, or
        has merge requests enabled.

        :param group_id: id of the GitLab group

        :return: a list of repositories including url, has_issues and fork.
        """

        url = urijoin(self.base_url, 'groups', group_id, 'projects')
        for repo in self._iter_repositories(url):
            yield repo
        url_subgroups = urijoin(self.base_url, 'groups', group_id, 'subgroups')
        for subgroup_id in self._iter_subgroups(url_subgroups):
            for repo in self.fetch_group_repositories(subgroup_id):
                yield repo

    def fetch_repositories(self):
        """Fetch GitLab repositories from a user, group or subgroup.

        This function fetch all the repositories from a GitLab user
        or a GitLab group and subgroups. It checks what type is the name
        requested and returns an iterator with the repositories.
        If the name does not exist in GitLab, it raises an exception.
        """
        name = self.org_name
        user_id = self.user_id(name)
        if user_id:
            return self.fetch_user_repositories(user_id)
        if '/' in name:
            name = name.replace('/', '%2F')

        group_id = self.group_id(name)
        if group_id:
            return self.fetch_group_repositories(group_id)

        raise Exception("GitLab owner %s not found" % name)

    def fetch_repos(self):
        """Fetch gitlab repositories.

        :return: a list of repos
        """
        repos = []
        for repo_data in self.fetch_repositories():
            repo = {}
            if repo_data['has_source']:
                repo[GIT] = repo_data['url']
            if repo_data['has_issues']:
                data_slice = repo_data['url'].split('/')
                repo[GITLAB_ISSUE] = '/'.join(data_slice[:4]) + '/' + '%2F'.join(data_slice[4:])
            if repo_data['has_merge']:
                data_slice = repo_data['url'].split('/')
                repo[GITLAB_MERGE] = '/'.join(data_slice[:4]) + '/' + '%2F'.join(data_slice[4:])

            if not repo:
                msg = "Unexpected repository. No issues, no source code {}".format(repo_data['url'])
                logging.error(msg)
                continue

            repos.append(repo)
        return repos


class GerritRepositories:
    """Gerrit backend to fetch repositories."""

    def __init__(self, data):
        """Creates a Gerrit backend

        :param data:
                       org: organization
                   api_url: gerrit API URL
                   opendev: gerrit OpenDev instance True | False
            filter_raw_url: Filter raw URL
                      user: gerrit username
                  password: gerrit password
                    rename: gerrit rename URL
        """
        self.api_url = data['api_url']
        self.user = data['user']
        self.password = data['password']
        self.filter_raw_url = data['filter_raw_url']
        self.opendev = data['opendev']
        self.rename = data['rename']
        self.org_name = data['org_name']

        if self.opendev and not self.filter_raw_url:
            msg = "Gerrit requires the param `--filter-raw-url`"
            logging.error(msg)
            sys.exit(-1)

    def derive_filter_raw_repo(self, url):
        """Derive the filtered raw repo from an URL and the filter-raw URL

        :param url: repo URL
        """
        repo = '/'.join(url.split('/')[-2:])
        filter_raw_repo = FILTER_RAW_REPO.format(self.filter_raw_url, repo)

        return filter_raw_repo

    def fetch_repos(self):
        """Fetch gerrit repositories

        :return: list of repositories
        """
        def add_repos(data, filter_raw_url):
            repos = []

            for repo in data:
                new_repo = {
                    GIT: repo[REPO_HTML_URL]
                }
                if filter_raw_url:
                    new_repo[GERRIT] = self.derive_filter_raw_repo(repo[REPO_HTML_URL])
                repos.append(new_repo)

            return repos

        repos = []

        auth = None
        if self.user and self.password:
            auth = (self.user, self.password)

        if self.opendev:
            url = ORG_REPOS.format(self.api_url, self.org_name)
        else:
            url = GERRIT_PROJECTS_URL.format(self.api_url)
        r = fetch(url, auth=auth)

        try:
            data = r.json()
        except simplejson.errors.JSONDecodeError:
            data_raw = r.text.split('\n', 1)[1]
            data_json = json.loads(data_raw)
            data = [{REPO_HTML_URL: self.rename + repo} for repo in data_json]
        repos.extend(add_repos(data, self.filter_raw_url))

        return repos


def remove_missing_repos(updated_project:dict, project_name:str, source_type:str,
                         new_repos:list, action=KEEP):
    """Remove the repositories missing from the projects file

    :param updated_project: The project sources configuration to be updated
    :param project_name: The project name (only for reference in logged messages)
    :param source_type: The source_type to be updated
    :param new_repos: New list of repositories from a backend
    :param action: What to do with the missing repositories.
    """

    # Check input
    if action not in (KEEP, EXCLUDE, CLEAN):
        print('Error: Unknown action "{}" for missing repositories.'.format(action))
        return
    if KEEP == action:
        return
    if {} == updated_project:
        print('Warning: No repo to remove in project "{}".'.format(project_name))
        return

    # Create a set of new repositories for easy comparison
    new_repos_set = set()
    target_data_sources = set()
    previous_data_sources = list(updated_project.keys())
    github_sources = [ds for ds in previous_data_sources if ds.startswith('github')]
    for repo in new_repos:
        for data_source, repo_in_data_source in repo.items():
            # target_data_sorces: filter-in previous or 'github'
            if data_source in previous_data_sources:
                target_data_sources.add(data_source)
                add_repo = True
            elif data_source == 'github' and source_type == GITHUB:
                target_data_sources.update(github_sources)
                add_repo = True
            else:
                add_repo = False

            if add_repo:
                # new_repos_set: all and .git-complementary
                new_repos_set.add(repo_in_data_source)
                if repo_in_data_source.endswith('.git'):
                    new_repos_set.add(repo_in_data_source[:-4])
                else:
                    new_repos_set.add(repo_in_data_source + '.git')

    # Handle git appart
    git_repos = updated_project['git']

    # Iterate through each data source to remove/exclude repos not in the list
    for ds in target_data_sources:
        if 'git' == ds:
           continue
        previous_repos = updated_project.get(ds, [])    # [] only for 'github'
        updated_project[ds] = []
        for repo in previous_repos:
            if repo in new_repos_set:
                updated_project[ds].append(repo)
            elif repo.split()[0] in new_repos_set:
                # When it contains 'url --xxx'
                updated_project[ds].append(repo)
            elif '--filter-no-collection=true' in repo and action == EXCLUDE:
                # Exclude it, but already has '--filter-no-collection=true'
                updated_project[ds].append(repo)
            elif action == EXCLUDE:
                print("Mark '{}/{}/{}' as excluded".format(project_name, ds, repo))
                aux = FILTER_NO_COLLECTION.format(repo)
                updated_project[ds].append(aux)
                try: # to mark its .git-complementary too
                     aux = '{}.git'.format(repo)
                     print("Mark '{}/{}/{}' as excluded".format(project_name, 'git', aux))
                     if aux in updated_project['git']:
                         updated_project['git'].remove(aux)
                     aux = FILTER_NO_COLLECTION.format(aux)
                     if aux not in updated_project['git']:
                         updated_project['git'].append(aux)
                except ValueError:
                    # It might have already been marked by a sibling datasource
                    pass
            else:
                print("Removing '{}/{}/{}'".format(project_name, ds, repo))
                try: # to remove its .git-complementary too
                    git_repos.remove('{}.git'.format(repo))
                except ValueError:
                    # It might have already been deleted by a sibling datasource
                    pass



def add_new_repos(updated_project, sourcedata, repos):
    """Update the projects.json file with the new repositories.

    If `user` and `token` are not None, the git repository will add the credentials.
    Add `git` datasource along each source_type.

    Pending: This function should not know about internals of specific source types.

    :param updated_project: project dict to update
    :param sourcedata: Data source to be updated.
                 user: User id for data retrieval from this data source (optional)
                token: Token for data retrieval from this data source (optional)
    :param repos: a list of repos of the following shape
        [
            {
                'git': ...,
                'github/gerrit/gitlab': ...
            },
            ...
        ]
    """

    # The name of the github issue backend name, usually is 'github',
    # but 'github:issue' is also very common
    data_sources = list(updated_project.keys())
    github_issue = GITHUB_ISSUE if ((GITHUB not in data_sources) and
                                    (GITHUB_ISSUE in data_sources)) else GITHUB

    # Credentials to add to the git repositories
    user  = sourcedata.get('user' , None)
    token = sourcedata.get('token', None)

    for repo in repos:
        for data_source in repo.keys():
            repo_in_data_source = repo[data_source]

            # if the data source is `git`, check that
            # (i) the repo ending with .git and (ii) the repo not ending in .git
            # don't exist in the projects.json. If this is the case, add the repo
            # ending with .git to the projects.json. This implies that the repos not
            # ending in .git will be kept, while new repos will be always added with
            # the .git extension. This logic is needed to avoid updating the projects.json
            # and re-collect the git raw data for the repos not ending with .git (note that
            # the re-collection would be required since for the same repo, the uuids differ
            # when it ends with/without .git)
            if data_source == GIT:
                if GIT not in updated_project:
                    updated_project[GIT] = []
                repo_git = repo_in_data_source
                if user and token:
                    repo_git = add_credentials(repo_in_data_source, user, token)
                if not exist_repo(repo_git, updated_project[data_source]):
                    updated_project[data_source].append(repo_git + GIT_EXTENSION)
            elif data_source == GITLAB_ISSUE:
                if GITLAB_ISSUE not in updated_project:
                    updated_project[GITLAB_ISSUE] = []
                if not exist_repo(repo_in_data_source,
                                  updated_project[GITLAB_ISSUE]):
                    updated_project[GITLAB_ISSUE].append(repo_in_data_source)
            elif data_source == GITLAB_MERGE:
                if GITLAB_MERGE not in updated_project:
                    updated_project[GITLAB_MERGE] = []
                if not exist_repo(repo_in_data_source,
                                  updated_project[GITLAB_MERGE]):
                    updated_project[GITLAB_MERGE].append(repo_in_data_source)
            elif data_source == GITHUB:
                if github_issue not in updated_project:
                    updated_project[github_issue] = []
                if not exist_repo(repo_in_data_source,
                                  updated_project[github_issue]):
                    updated_project[github_issue].append(repo[data_source])
            else:
                if data_source not in updated_project:
                    updated_project[data_source] = []
                if not exist_repo(repo_in_data_source,
                                  updated_project[data_source]):
                    updated_project[data_source].append(repo[data_source])

    # Update the repos if the name of the data source contains the word "github"
    # with the data source "github".
    # i.e. github:prs, github:pull, github2, githubql, etc.
    # They will have the same repos as "github"
    for ds in data_sources:
        if GITHUB not in ds or ds == github_issue:
            continue
        updated_project[ds] = updated_project[github_issue]


def sync_project(destination, sourcedata, on_removed, dry_run):
    """Sync the GitHub or Gerrit repos together with their corresponding Git
    repos in the projects.json.

    :param destination: Parameters for the storage of the projects.json file.
                 token: GitLab repossync bot token
            project_id: GitLab project ID of the projects.json file
          project_name: the name of the project to update in the projects.json

    :param sourcedata: Data for querying the source of updates.
    :param on_removed: Reaction policy for upstream-removed repositories
    :return: commit message
    """

    dt = GitLabFileManager(destination['token'])

    # load projects.json
    projects_json_raw, branch = dt.load_file(destination['project_id'], PROJECTS_JSON)
    projects_json = json.loads(projects_json_raw)

    # fetch the upstream repos
    be = {
        (GITHUB): GitHubRepositories,
        (GITLAB): GitLabRepositories,
        (GERRIT): GerritRepositories
    }
    backend = be[sourcedata['source_type']](sourcedata)
    repos = backend.fetch_repos()

    # update the project repos
    updated_projects = copy.deepcopy(projects_json)
    project_name = destination['project_name']

    target = updated_projects[project_name] if project_name in updated_projects.keys() else {}
    remove_missing_repos(target, project_name, sourcedata['source_type'], repos, action=on_removed)
    add_new_repos(target, sourcedata, repos)

    updated_projects[project_name] = target

    # update the projects.json
    updated_projects_json_raw = json.dumps(updated_projects, indent=4, sort_keys=True)

    # commit the changes
    commit_msg = COMMIT_MSG.format(project_name)
    if dry_run:
        commit_msg += json.dumps(target, indent=4, sort_keys=True)
    else:
        dt.upload_file(destination['project_id'], PROJECTS_JSON, updated_projects_json_raw, commit_msg, branch)
    return commit_msg


def parse_args():
    """Parse command line arguments."""

    DESCRIPTION = """
    Sync the GitHub, GitLab, or Gerrit repos together with their corresponding
    Git repos in the projects.json.

    This script retrieves the projects.json located in GitLab, and depending
    on the `source` (i.e., GitHub, GitLab, or Gerrit) and the `org_name`
    selected, it fetches the repos from the GitHub or OpenDev APIs. Then, it
    updates the projects.json with the new repos for the given `org_name`, and
    finally commits the changes to the GitLab repository hosting the
    projects.json with the commit message `COMMIT_MSG`.
    Note that in case the `source` is GitHub and the section `github:repo` is
    contained in the projects.json, the repos in that section are
    automatically updated.
    """

    EPILOG = """
To add the credentials to the git repositories it is mandatory to enable
`--github-token` and `--github-user` for GitHub and `--gitlab-token` and
`--gitlab-user` for GitLab.

Removed repositories upstream can be kept, excluded or cleaned from the
projects.json file using the `--removed` option.

All GitHub repositories starting with `-` are skipped.

Examples of execution calls:
    reppossync
        --bot-token ...
        --gitlab-project-id ...
        --org-name starlingx
        --project-name StarlingX
        --filter-raw-url review.opendev.org
        --source gerrit
        --gerrit-api-url https://opendev.org/api/v1
        --gerrit-opendev true

    repossync
        --bot-token ...
        --gitlab-project-id ...
        --org-name wikimedia
        --project-name Wikimedia
        --filter-raw-url gerrit.wikimedia.org
        --source gerrit
        --gerrit-api-url https://gerrit.wikimedia.org/r/a
        --gerrit-rename https://my.gerrit.org (optional)
        --gerrit-user ... (optional)
        --gerrit-password ... (optional)

    repossync
        --bot-token ...
        --gitlab-project-id ...
        --org-name Bitergia
        --project-name Bitergia
        --source github
        --github-token ... (optional)
        --github-user ... (optional)
        --gitlab-token ... (optional)
        --gitlab-user ... (optional)
    """
    parser = argparse.ArgumentParser(
        description = DESCRIPTION,
        epilog = EPILOG,
        formatter_class = argparse.RawTextHelpFormatter
    )
    parser.add_argument("--bot-token",
                        help="Repossync bot GitLab token. Mandatory")
    parser.add_argument("--dry-run", action='store_true',
                        help="Execute without changes")
    parser.add_argument("--org-name",
                        help="Organization to sync. Mandatory")
    parser.add_argument("--project-name",
                        help="Project name in the projects.json. Mandatory")
    parser.add_argument("--source", choices=[GERRIT, GITHUB, GITLAB],
                        help="Data source to sync. Mandatory")
    parser.add_argument("--github-token", default=None,
                        help="GitHub token")
    parser.add_argument("--github-user", default=None,
                        help="GitHub user. This needs --github-token to add credentials to git URL")
    parser.add_argument("--gitlab-project-id",
                        help="GitLab project id. Mandatory")
    parser.add_argument("--github-search", default=None,
                        help="Sync repository by GitHub query search")
    parser.add_argument("--archived", choices=[True, False],
                        help="Sync repository by GitHub query search")
    parser.add_argument("--forks", choices=[True, False],
                        help="Sync repository by GitHub query search")
    parser.add_argument("--gitlab-token", default=None,
                        help="GitLab token")
    parser.add_argument("--gitlab-domain", default=None,
                        help="GitLab domain. This needs --gitlab-token to add credentials to git URL")
    parser.add_argument("--gitlab-user", default=None,
                        help="GitLab user. This needs --gitlab-token to add credentials to git URL")
    parser.add_argument("--filter-raw-url", default=None,
                        help="Filter-raw URL (for Gerrit)")
    parser.add_argument("--gerrit-user", default=None,
                        help="Gerrit user")
    parser.add_argument("--gerrit-password", default=None,
                        help="Gerrit password")
    parser.add_argument("--gerrit-api-url", default=None,
                        help="Gerrit API URL")
    parser.add_argument("--gerrit-rename", default=None,
                        help="Gerrit rename URL")
    parser.add_argument("--gerrit-opendev", choices=[True, False],
                        default=False, help="Gerrit OpenDev instance")
    parser.add_argument("--removed", choices=[KEEP, EXCLUDE, CLEAN], default=KEEP,
                        help="Remove upstream-removed repositories (default: %(default)s)")

    return parser.parse_args()


if __name__ == '__main__':
    """Parses the CLI input and calls the execution"""
    args = parse_args()

    # Mandatory args:
    if None in (args.bot_token, args.gitlab_project_id, args.org_name, args.project_name, args.source):
        print('FATAL: --bot-token, --gitlab-project-id, --org-name, --project-name and --source are mandatory.')
        sys.exit(2)

    source_type = args.source.lower()

    # Target data:
    source_data = {
        'org_name': args.org_name,
        'source_type': source_type
    }

    # Source data:
    if source_type == GITHUB:
        add = {
            'search': args.github_search,
            'token': args.github_token,
            'user': args.github_user
        }
    elif source_type == GITLAB:
        add = {
            'domain': args.gitlab_domain,
            'token': args.gitlab_token,
            'user': args.gitlab_user,
            'archived': args.archived,
            'forks': args.forks
        }
    elif source_type == GERRIT:
        add = {
            'filter_raw_url': args.filter_raw_url,
            'user': args.gerrit_user,
            'password': args.gerrit_password,
            'api_url': args.gerrit_api_url,
            'rename': args.gerrit_rename,
            'opendev': args.gerrit_opendev
        }
    else:
        raise Exception('Unknown source type'.format(source_type))

    source_data.update(add)

    # Execution:
    print('Syncing project', args.project_name, '...')
    destination = {
        'token': args.bot_token,
        'project_id': args.gitlab_project_id,
        'project_name': args.project_name,
    }
    commit_msg = sync_project(
        destination,
        source_data,
        on_removed=args.removed,
        dry_run=args.dry_run
    )
    msg = "\t{}".format(commit_msg)
    print(msg)
