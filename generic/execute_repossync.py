# -*- coding: utf-8 -*-
# SPDX-License-Identifier: GPL-3.-or-later
#
# Copyright (C) 2015-2025 Bitergia
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Authors:
#   Quan Zhou <quan@bitergia.com>
#   Igor Zubiaurre <izubiaurre@bitergia.com>
#


import argparse
import json
import os
import sys

from repossync import sync_project, GITHUB, GITLAB, GERRIT

sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)), "/usr/local/lib/jenkins_ticket_utils/"))


def parse_args():
    HELP = """
  Executes the script repossync.py to synchronize the projects.json.
  Supports the following data sources:
      - github: it will sync automatically 'github:repo' if does exists
      - gerrit
      - gitlab
    """

    EPILOG = """
input formats:

    JSON file with configuration parameters:
    {
        "jenkins_host": "https://myjenkinshost/",
        "bot_token": "<GitLab-Token>",
        "gitlab_token": "<GitLab-Token>", (optional)
        "gitlab_user": "<GitLab-User>", (optional)
        "github_token": "<GitHub-Token>", (optional, but recommended)
        "github_user": "<GitHub-User>" (optional)
    }

    JSON file with projects information:
    {
        "<project>": {
            "project_id": "<gitlab_project_id>",
            "archived": False, (optional)
            "forks": True, (optional)
            "github": [
                {
                    "alias": "Bitergia",
                    "name": "targeted_github_organization",
                    "token": "<GitHub-Token>", (optional)
                    "user": "<GitHub-User>", (optional)
                    "archived": True, (optional)
                    "forks": False (optional)
                }
            ],
            "gerrit": [
                {
                    "alias": "Chaoss",
                    "name": "chaoss",
                    "filter_raw_url": "review.opendev.org",
                    "api_url": "https://opendev.org/api/v1",
                    "is_opendev": true
                },
                {
                    "alias": "Bitergia-Gerrit",
                    "name": "bitergia-gerrit",
                    "user": "<gerrit-user>",
                    "password": "<gerrit-password>",
                    "api_url": "https://gerrit.wikimedia.org/r/a"
                }
            ],
            "gitlab": [
                {
                    "alias": "Bitergia",
                    "name": "path/to/targeted/gitlab/subgroup",
                    "api_url": "https://gitlab.eclipse.org", (optional)
                    "token": "<GitLab-Token>", (optional)
                    "user": "<GitLab-User>", (optional)
                    "archived": True (optional)
                }
            ]
        }
    }

 important:
    You have to clone the repo https://gitlab.com/Bitergia/devops/ticket-utils
    at `/usr/local/lib/jenkins_ticket_utils`
    `git clone https://gitlab.com/Bitergia/devops/ticket-utils /usr/local/lib/jenkins_ticket_utils`
    """

    parser = argparse.ArgumentParser(
        description = HELP,
        epilog = EPILOG,
        formatter_class = argparse.RawTextHelpFormatter
    )
    parser.add_argument('conf',
                        help="JSON config file")
    parser.add_argument('projects',
                        help="JSON projects file")
    parser.add_argument('--dry-run', action='store_true',
                        help="Simulate execution (without saving changes)")
    args = parser.parse_args()

    return args


def read_json(filename):
    with open(filename) as json_file:
        data = json.load(json_file)
    return data


def execute_sync_project(project, source_type, gl_project_id, bot_token, gl_token,
                         gl_user, gh_token, gh_user, dry_run):
    """ Execute sync_project of the script repossync.py with the parameters

    :param project: project
    :param source_type: github | gerrit | gitlab
    :param gl_project_id: gitlab project id
    :param bot_token: gitlab repossync bot token
    :param gl_token: gitlab token
    :param gl_user: gitlab user
    :param gh_token: github token
    :param gh_user: github user
    :param dry_run: Simulate execution (without saving changes)

    :return: commit messages
    """
    msg = ""
    for group in project[source_type]:
        api_url = group.get('api_url', project.get('api_url', None))
        alias = group['alias']

        source_data = {
            'org_name': group.get('name', None),
            'source_type': source_type
        }
        if source_type == GITHUB:
            add = {
                'search': group.get('query', None),
                'token': group.get('token', gh_token),
                'user': group.get('user', gh_user),
                'archived': group.get('archived', project.get('archived', True)),
                'forks': group.get('forks', project.get('forks', False))
            }
        elif source_type == GITLAB:
            domain = api_url.split('//')[1] if api_url else None
            add = {
                'domain': domain,
                'token': group.get('token', gl_token),
                'user': group.get('user', gl_user),
                'archived': group.get('archived', project.get('archived', True)),
                'forks': group.get('forks', project.get('forks', False))
            }
        elif source_type == GERRIT:
            add = {
                'filter_raw_url': group.get('filter_raw_url', None),
                'user': group.get('user', None),
                'password': group.get('password', None),
                'api_url': api_url,
                'rename': group.get('rename', None),
                'opendev': group.get('is_opendev', False)
            }
        else:
            raise Exception('Unknown source type "{}"'.format(source_type))
        source_data.update(add)

        print("\t{}".format(alias))
        destination = {
            'token': bot_token,
            'project_id': gl_project_id,
            'project_name': alias
        }
        msg_group = sync_project(
            destination,
            source_data,
            on_removed=group.get('removed', project.get('removed', 'keep')),
            dry_run=dry_run
        )

        print("\t{}\n".format(msg_group))
        msg += "\t{}\n".format(msg_group)
    return msg


def main():
    PARAMETERS = ('project_id', 'archived', 'forks', 'removed')
    args = parse_args()

    conf = read_json(args.conf)
    bot_token = conf["bot_token"]
    gl_token = conf.get('gitlab_token', None)
    gl_user = conf.get('gitlab_user', None)
    gh_token = conf.get("github_token", None)
    gh_user = conf.get('github_user', None)

    projects = read_json(args.projects)
    msg = ""
    for project in projects:
        print("Project: {}".format(project))
        gl_project_id = projects[project]["project_id"]
        data_sources = [source_type for source_type in projects[project] if not source_type in PARAMETERS]
        msg += "{}\n".format(project)
        for data_source in data_sources:
            msg += execute_sync_project(projects[project], data_source, gl_project_id,
                                        bot_token, gl_token, gl_user, gh_token, gh_user, args.dry_run)


if __name__ == '__main__':

    main()
