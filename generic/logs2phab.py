# Copyright (C) 2015-2020 Bitergia
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Authors:
#     Quan Zhou <quan@bitergia.com>
#


import argparse
import json
import os
import re
import socket
import sys

from elasticsearch import Elasticsearch, RequestsHttpConnection

sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)), "/usr/local/lib/jenkins_ticket_utils/"))
from ticket_utils.phabricatorUtils import PhabricatorUtils


ERROR_ES_CONNECTION = "Can't connect to Elastic Search"
ERROR_ES_INDEX = "Can't create"
ERROR_GERRIT_SSH = "gerrit cmd ssh  -p 29418"
ERROR_GROUPSIO = "Download archive permission disabled"
ERROR_SH = "[sortinghat] Error in command"
ERROR_STUDY = "Problem executing study"
ERROR_TASK_MANAGER = "Exception in Task Manager"
INVALID_LOGS = [
    "<class 'json.decoder.JSONDecodeError'>"
]
INVALID_GITHUB_EVENT_LOGS = "Events not collected for issue"
MAX_PRIORITY = 5
MAX_RETRY = 5
QUERY = """{
    "query": {
        "bool": {
            "must": [
                {
                    "match": {
                        "type": "ERROR"
                    }
                }
            ],
            "filter": [
                {
                    "range": {
                        "@timestamp": {
                            "gte": "%s"
                        }
                    }
                }
            ]
        }
    }
}
"""
STRIP_SORTINGHAT_PARAMS = 19
TIME_RANGE = "now-1h"
TITLE = '[logs.bitergia.net] {} - Priority {}'
USELESS_LINES = [
    " (return code: 128)",
    "fatal: could not read Username for 'https://github.com': No such device or address",
    "fatal: could not read Username for 'https://gitlab.com': No such device or address",
    "fatal: The remote end hung up unexpectedly"
]


def main():
    """To fetch logs in ElasticSearch and create tickets in phabricator for each type of
    errors found and set a priority depending on your configuration file (by default
    is 5), the priorities are ordered from lowest(1) to highest(5).

    If you want to change the time range you have to set the parameter `time` in
    the configuration file, by default is `now-1h`.

    JSON configuration file:
    ```
    {
        "es_host": "http://localhost:9200",
        "index": "index_name",
        "time_range": "now-1d",
        "phab_host": "https://yourphabhost/api/",
        "phab_token": "api-phabtoken",
        "phab_phid": ["PHID-PROJ-phab_phid"],
        "priorities": {
            "requests.exceptions.HTTPError": 1,
            "dulwich.errors.GitProtocolError": 2
        }
    }
    ```
    i.e. phabricator ticket:
    Title: [logs.bitergia.net] KeyError - Priority 5

    Description:
    This error has been found in the environments: ['bitergia']
    ```lines=10
    Error enriching raw from meetup (https://meetup.com/): 'lat'
    Traceback (most recent call last):
      File "/usr/local/lib/python3.5/dist-packages/grimoire_elk/elk.py", line 533, in enrich_backend
        enrich_count = enrich_items(ocean_backend, enrich_backend)
      File "/usr/local/lib/python3.5/dist-packages/grimoire_elk/elk.py", line 321, in enrich_items
        total = enrich_backend.enrich_items(ocean_backend)
      File "/usr/local/lib/python3.5/dist-packages/grimoire_elk/enriched/meetup.py", line 355, in enrich_items
        eitem = self.get_rich_item(item)
      File "/usr/local/lib/python3.5/dist-packages/grimoire_elk/enriched/enrich.py", line 93, in decorator
        eitem = func(self, *args, **kwargs)
      File "/usr/local/lib/python3.5/dist-packages/grimoire_elk/enriched/meetup.py", line 214, in get_rich_item
        "lat": event['venue']['lat'],
    KeyError: 'lat'
    ```
    """
    args = parse_args()
    conf_file = args.conf

    conf = read_json(conf_file)
    es_host = conf['es_host']
    index = conf['index']
    phab_host = conf['phab_host']
    phab_token = conf['phab_token']
    phab_phid = conf['phab_phid']
    priorities = conf['priorities']

    query = QUERY % conf['time_range'] if 'time_range' in conf else QUERY % TIME_RANGE

    es = Elasticsearch([es_host], retry_on_timeout=True, timeout=100,
                       verify_certs=False, connection_class=RequestsHttpConnection)
    response = es.search(index=index, body=json.loads(query), scroll='2m', size=1000)

    print("Got {} Hits:".format(response['hits']['total']))

    errors = fetch_errors(response)
    phabricator(phab_host, phab_token, phab_phid, errors, priorities)

    print("The number of types of errors: {}".format(len(errors)))


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('conf',
                        help='PHAB configuration file')
    args = parser.parse_args()

    return args


def read_json(filename):
    with open(filename) as json_file:
        data = json.load(json_file)
    return data


def fetch_errors(res):
    """Fetch errors from ElasticSearch.

    :param res: Elasticsearch response

    :return: dict
    """
    errors = {}
    for hit in res['hits']['hits']:
        source = ""
        message = hit['_source']['message']
        if message in INVALID_LOGS or INVALID_GITHUB_EVENT_LOGS in message:
            continue
        last_line = message.split('\n')[-1]
        if last_line in USELESS_LINES:
            last_line = message.split('\n')[-2]

        if ERROR_TASK_MANAGER in message:
            error = [ERROR_TASK_MANAGER, last_line.split(':')[0]]
        elif "Traceback" in message:
            error = last_line.split(':')
            if ERROR_GROUPSIO in last_line:
                error = [last_line.split(':')[0], "groupsio"]
            first_line = message.split('\n')[0]
            source = re.findall("^Error [a-z]* [a-z]* from .* \(([^()]+)\)", first_line)[0]
            if len(error) == 1:
                error = [error[0], error[0]]
        elif ERROR_GERRIT_SSH in last_line:
            k_type = last_line.split("@")[1].split()[0]
            error = [ERROR_GERRIT_SSH, k_type]
        elif ERROR_SH in last_line:
            # STRIP_SORTINGHAT_PARAMS is the position of the SortingHat command
            k_type = last_line.split("'")[STRIP_SORTINGHAT_PARAMS]
            error = [ERROR_SH, k_type]
        elif ERROR_GROUPSIO in last_line:
            # This error is skipped due to in the next log we have the same error
            # but with traceback
            continue
        elif ERROR_ES_CONNECTION in last_line:
            error = [ERROR_ES_CONNECTION, "ElasticSearch"]
        elif ERROR_ES_INDEX in last_line:
            if "data/" in last_line:
                k_type = last_line.split("data/")[1].split()[0]
            else:
                k_type = last_line.split(":9200/")[1].split()[0]
            error = [ERROR_ES_INDEX + " index", k_type]
        elif ERROR_STUDY in last_line:
            k_type = last_line.split(ERROR_STUDY)[1].split(",")[0]
            error = [ERROR_STUDY, k_type]
        else:
            error = last_line.split(':')
        key = error[0]
        key_type = error[1]
        env = hit['_source']['environment']
        new_message = {
            'envs': [env],
            'message': message,
            'sources': [source]
        }
        new_key_type = {
            key_type: new_message
        }
        new = {
            'key_type': new_key_type,
        }

        if key not in errors:
            errors[key] = new
            continue

        key_types_found = errors[key]['key_type']
        if key_type not in key_types_found:
            key_types_found[key_type] = new_message
        else:
            envs = key_types_found[key_type]['envs']
            envs.append(env) if env not in envs else envs

            sources = key_types_found[key_type]['sources']
            sources.append(source) if source != "" and source not in sources else sources

    return errors


def phabricator(host, token, phid, errors, priorities):
    """Create or update tickets in phabricator.

    :param host: PHAB host
    :param token: PHAB token
    :param phid: PHAB PHID
    :param errors: dict errors
    :param priorities: dict error priorities
    """

    phab = PhabricatorUtils(host, token)

    for e in errors:
        priority = MAX_PRIORITY
        if e in priorities:
            priority = priorities[e]
        title = TITLE.format(e, priority)
        phab_msg = ""
        for k in errors[e]['key_type']:
            k_types = errors[e]['key_type'][k]
            envs = k_types['envs']
            message = k_types['message']
            sources = '\n'.join(k_types['sources'])
            phab_msg += "This error has been found in the environments: {}" \
                        "\n```lines=10\n{}\n```\n".format(envs, message)
            if sources:
                phab_msg += "Sources affected:\n```lines=5\n{}\n```\n".format(sources)

        i = 0
        while i < MAX_RETRY:
            try:
                result = phab.get_search_results(title, phid)
                task_id = [result[task]['id'] for task in result if result[task]['title'] == title][0]
                phab.update_task(task_id, phab_msg)
                print("The ticket {} has been updated: {}".format(task_id, title))
                break
            except (IndexError, TypeError):
                phab.create_task(title, phab_msg, phid)
                print("New error created in PHAB: {}".format(title))
                break
            except socket.timeout as e:
                i += 1
                msg = "{}\nRetrying {} times of 5".format(e, i)
                print(msg)


if __name__ == '__main__':
    main()
