#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2019 Bitergia
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# Authors:
#   Miguel Ángel Fernández <mafesan@bitergia.com>
#

import argparse
import json
import logging
import sys


DESC_MSG = 'Updates the "meetup" section under a given project\
            in a sources file'


def main(args):

    logger.info("update_meetup_groups start")

    # Load input sources file
    with open(args.projects_filepath, "r") as input_file:
        projects = json.load(input_file)

    # Get list of existing meetups
    try:
        meetup_list = projects[args.project_name]["meetup"]
    except Exception as e:
        logger.error("Project '%s' was not found on the sources file"
                     % args.project_name)
        sys.exit(1)

    # Meetups to be added. The format is one Meetup URL per line
    with open(args.new_meetups_filepath, "r") as newfile:
        new_meetups = newfile.readlines()

    new_list = []

    for meetup_url in new_meetups:
        if "www.meetup.com/" not in meetup_url:
            logger.warning("Wrong entry: %s" % meetup_url)
            continue

        meetup_name = get_meetup_name(meetup_url)

        if meetup_name not in meetup_list:
            new_list.append(meetup_name)

    updated_list = sorted(meetup_list + new_list)
    projects[args.project_name]["meetup"] = updated_list

    with open(args.output_filepath, "w") as output_file:
        json.dump(projects, output_file, indent=4, sort_keys=True)

    # Brief report with stats after the execution
    logger.debug("Number of meetups before: %s" % len(meetup_list))
    logger.debug("Number of meetups in new list: %s" % len(new_meetups))
    logger.debug("Number of new meetups: %s " % len(new_list))
    logger.debug("Number of total meetups now: %s" % len(updated_list))

    if args.print_new_list:
        logger.info("Priniting list of new meetups:\n")
        for new_meetup in new_list:
            print(new_meetup)


def get_meetup_name(meetup_url):
    """Extract Meetup name from a given URL

    :param meetup_url: URL for a Meetup group
    :return: Meetup name
    """

    if meetup_url[-1] == '\n':
        meetup_url = meetup_url[:-1]

    if meetup_url[-1] == '/':
        meetup_url = meetup_url[:-1]

    meetup_name = meetup_url.split("/")[-1]

    return meetup_name


logger = logging.getLogger(__name__)


def configure_logging(log_file, debug_mode_on=False):
    """Set up the logging and returns a list with the file descriptors

    :param log_file: Path for the log file
    :param debug_mode_on: If True, the level of the logger will be DEBUG
    :return List with logging file descriptors
    """

    if debug_mode_on:
        logging_mode = logging.DEBUG
    else:
        logging_mode = logging.INFO

    logger = logging.getLogger()
    logger.setLevel(logging_mode)

    # redirect logging to our log file
    fh = logging.FileHandler(log_file, 'a')
    fh.setLevel(logging_mode)

    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging_mode)

    # create formatter and add it to the handlers
    formatter = logging.Formatter("[%(asctime)s - %(levelname)s] %(message)s")
    fh.setFormatter(formatter)
    ch.setFormatter(formatter)

    logger.addHandler(fh)
    logger.addHandler(ch)

    keep_fds = [fh.stream.fileno()]

    return keep_fds


def parse_args():
    """Parse arguments from the command line"""

    parser = argparse.ArgumentParser(description=DESC_MSG)

    parser.add_argument('-i', '--input-file', dest='projects_filepath',
                        required=True, help='Path for input sources file')
    parser.add_argument('-p', '--project-name', dest='project_name',
                        required=True, help='Name of the project where\
                        the meetups will be updated')
    parser.add_argument('-m', '--meetup-list', dest='new_meetups_filepath',
                        required=True, help='Path for new Meetup groups list.\
                        Format: list of Meetup URLs')
    parser.add_argument('-o', '--output-file', dest='output_filepath',
                        required=True, help='Path for updated sources file')
    parser.add_argument('-l', '--print-new-list', dest='print_new_list',
                        action='store_true', default=False,
                        help='Prints list of new meetups which were not\
                        in the input sources file')
    parser.add_argument('-g', '--debug', dest='debug_mode_on',
                        action='store_true', default=False,
                        help='Enables debug mode')
    parser.add_argument('--log-file', dest='log_file',
                        default='update_meetup_groups.log', required=False,
                        help='Path to log file')

    return parser.parse_args()


if __name__ == '__main__':
    try:
        args = parse_args()
        keep_fds = configure_logging(args.log_file, args.debug_mode_on)
        main(args)
    except Exception as e:
        logger.exception("Exception message:")
        s = "Error: %s update_meetup_groups is exiting now." % str(e)
        logger.error(s)
        sys.exit(1)
