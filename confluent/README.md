# Procedure

We agreed with Confluent to help them on updating their Meetup groups list on their projects file.

They use a spreadsheet which contains a column with a list of Meetup groups. It is updated on a weekly or monthly basis.

Every week or month, Confluent people will open an issue on their support tracker pasting the content of the column I mentioned before inside a new ticket.

This is a list of URLs corresponding to Meetup groups, however we may encounter non-Meetup URLs. We use the pasted text to compose a TXT file only with a list of Meetup group URLs. Although the script ignores non-URLs (the idea is to improve these URL checks in future versions), the URL list must be reviewed before executing the script to avoid further errors.

# Scripts

## `update_meetup_groups.py`
The aim of the script `update_meetup_groups.py` is to update the list of Meetup groups under the project `Confluent`
from [Confluent's sources file](https://gitlab.com/Bitergia/c/Confluent/sources/blob/master/projects.json).

### Usage

```
usage: update_meetup_groups.py [-h] -i PROJECTS_FILEPATH -p PROJECT_NAME -m
                               NEW_MEETUPS_FILEPATH -o OUTPUT_FILEPATH [-l]
                               [-g] [--log-file LOG_FILE]

Updates the "meetup" section under a given project in a sources file

optional arguments:
  -h, --help            show this help message and exit
  -i PROJECTS_FILEPATH, --input-file PROJECTS_FILEPATH
                        Path for input sources file
  -p PROJECT_NAME, --project-name PROJECT_NAME
                        Name of the project where the meetups will be updated
  -m NEW_MEETUPS_FILEPATH, --meetup-list NEW_MEETUPS_FILEPATH
                        Path for new Meetup groups list. Format: list of
                        Meetup URLs
  -o OUTPUT_FILEPATH, --output-file OUTPUT_FILEPATH
                        Path for updated sources file
  -l, --print-new-list  Prints list of new meetups which were not in the input
                        sources file
  -g, --debug           Enables debug mode
  --log-file LOG_FILE   Path to log file
```

### Format

The Meetup groups list has to follow the following format:
* `.txt` file.
* 1 line per Meetup URL, usual format is `https://meetup.com/<meetup-name>\n`).

```
my-meetup-url1
my-meetup-url2
...
my-meetup-urlN
```

### Execution

```
$ python3 update_meetup_groups.py -i projects.json -m meetup-list.txt -p "Confluent" -o updated_projects.json -g
```
